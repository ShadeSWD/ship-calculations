# Ship calculations
- Created base classes for Ship, Rudder and Hull
- Realised creation of a hull from s2g file
- Attributes are enough to calculate efficiencies of the rudder
- Classes are ready to be used in other programs
- Attributes may be imported from json