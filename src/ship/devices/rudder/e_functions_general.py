from src.utils.ship_maths import interpolate


def interpolate_e(c_p: float, sigma: float) -> float:
    """
    Interpolates depending on prismatic longitudinal coefficient and returns efficiency

    :param c_p: prismatic longitudinal coefficient
    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """

    if (c_p >= 0.55) & (c_p < 0.65):
        e_1 = get_d_e(c_p=0.55, sigma=sigma)
        e_2 = get_d_e(c_p=0.65, sigma=sigma)
        e = interpolate(x1=0.55, y1=e_1, x2=0.65, y2=e_2, x3=c_p)
    elif (c_p >= 0.65) & (c_p < 0.75):
        e_1 = get_d_e(c_p=0.65, sigma=sigma)
        e_2 = get_d_e(c_p=0.75, sigma=sigma)
        e = interpolate(x1=0.65, y1=e_1, x2=0.75, y2=e_2, x3=c_p)
    elif (c_p >= 0.75) & (c_p < 0.85):
        e_1 = get_d_e(c_p=0.75, sigma=sigma)
        e_2 = get_d_e(c_p=0.85, sigma=sigma)
        e = interpolate(x1=0.75, y1=e_1, x2=0.85, y2=e_2, x3=c_p)
    elif (c_p >= 0.85) & (c_p <= 0.95):
        e_1 = get_d_e(c_p=0.85, sigma=sigma)
        e_2 = get_d_e(c_p=0.95, sigma=sigma)
        e = interpolate(x1=0.85, y1=e_1, x2=0.95, y2=e_2, x3=c_p)
    else:
        e = 0
    return round(e, 4)


def get_d_e(c_p: float, sigma: float) -> float:
    """
    Calculates efficiency of 1-propeller vessel rudder excl. tow, lifeboat and fishing vessels

    :param c_p: prismatic longitudinal coefficient
    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """

    if c_p == 0.55:
        e = e_55(sigma)
    elif c_p == 0.65:
        e = e_65(sigma)
    elif c_p == 0.75:
        e = e_75(sigma)
    elif c_p == 0.85:
        e = e_85(sigma)
    elif c_p == 0.95:
        e = e_95(sigma)
    else:
        e = 0
    return round(e, 4)


def e_55(sigma: float) -> float:
    """
    Calculates efficiency for c_p = 55

    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """

    if (sigma >= 0.78) & (sigma <= 0.9375):
        e = 0.0245 + (-0.415873 + (-1.57578 + (2.34682 + (-19.5563 + (-1970.83 + (-12471.8 + (300149.0 + (-1.65774 * (10 ** 7) + (-4.06943 * (10 ** 8) + (5.74153 * (10 ** 9) + (-1.85795 * (10 ** 11) + (1.5442 * (10 ** 13) + (2.97029 * (10 ** 14) + (-3.95374 * (10 ** 15) + (2.38027 * (10 ** 16) - 3.6688 * (10 ** 19) * (-0.84 + sigma)) * (-0.9 + sigma)) * (-0.82 + sigma)) * (-0.88 + sigma)) * (-0.85 + sigma)) * (-0.92 + sigma)) * (-0.8 + sigma)) * (-0.89 + sigma)) * (-0.79 + sigma)) * (-0.93 + sigma)) * (-0.83 + sigma)) * (-0.91 + sigma)) * (-0.81 + sigma)) * (-0.86 + sigma)) * (-0.78 + sigma)) * (-0.9375 + sigma)
    elif (sigma > 0.9375) & (sigma <= 1):
        e = 0.0815 + (1.08571 + (-8.44486 + (32.1 + (-1891.82 + (-1.37988 * (10 ** 6) + 1.58606 * (10 ** 8) * (-0.9545 + sigma)) * (-0.946 + sigma)) * (-0.98 + sigma)) * (-0.963 + sigma)) * (-0.9375 + sigma)) * (-0.99 + sigma)
    else:
        e = 0
    return e


def e_65(sigma: float) -> float:
    """
    Calculates efficiency for c_p = 65

    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """
    if (sigma >= 0.78) & (sigma <= 0.9460):
        e = 0.026 + (-0.433735 + (-1.70041 + (0.309227 + (-49.5739 + (335.695 + (15988.8 + (-414056.0 + (-1.23911 * (10 ** 7) + (3.45779 * (10 ** 8) + (4.69731 * (10 ** 9) + (-1.19092 * (10 ** 11) + (-4.75861 * (10 ** 10) + (-6.33311 * (10 ** 13) + (-1.82643 * (10 ** 15) + (8.53211 * (10 ** 16) + (-6.3973 * (10 ** 18) - 3.44444 * (10 ** 20) * (-0.89 + sigma)) * (-0.87 + sigma)) * (-0.82 + sigma)) * (-0.92 + sigma)) * (-0.84 + sigma)) * (-0.9 + sigma)) * (-0.8 + sigma)) * (-0.9375 + sigma)) * (-0.83 + sigma)) * (-0.88 + sigma)) * (-0.79 + sigma)) * (-0.93 + sigma)) * (-0.81 + sigma)) * (-0.91 + sigma)) * (-0.86 + sigma)) * (-0.78 + sigma)) * (-0.946 + sigma)
    elif (sigma > 0.9460) & (sigma <= 1):
        e = 0.072 + (1.04545 + (-10.2579 + (347.017 + (-2775.67 + 934261.0 * (-0.98 + sigma)) * (-0.9545 + sigma)) * (-0.969 + sigma)) * (-0.946 + sigma)) * (-0.99 + sigma)
    else:
        e = 0
    return e


def e_75(sigma: float) -> float:
    """
    Calculates efficiency for c_p = 75

    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """

    if (sigma >= 0.78) & (sigma <= 0.9545):
        e = 0.027 + (-0.469914 + (-2.07656 + (2.32212 + (-97.6196 + (-1676.58 + (21699.7 + (-226020.0 + (-2.91248 * (10 ** 6) + (9.13949 * (10 ** 7) + (-2.65519 * (10 ** 7) + (8.17106 * (10 ** 10) + (7.10766 * (10 ** 11) + (-6.49244 * (10 ** 13) + (1.36424 * (10 ** 15) + (-7.40829 * (10 ** 16) + (1.32919 * (10 ** 18) + (2.85383 * (10 ** 19) - 5.82245 * (10 ** 21) * (-0.84 + sigma)) * (-0.91 + sigma)) * (-0.86 + sigma)) * (-0.89 + sigma)) * (-0.81 + sigma)) * (-0.9375 + sigma)) * (-0.83 + sigma)) * (-0.92 + sigma)) * (-0.85 + sigma)) * (-0.79 + sigma)) * (-0.946 + sigma)) * (-0.9 + sigma)) * (-0.8 + sigma)) * (-0.93 + sigma)) * (-0.82 + sigma)) * (-0.87 + sigma)) * (-0.78 + sigma)) * (-0.9545 + sigma)
    elif (sigma > 0.9545) & (sigma <= 1):
        e = 0.063 + (1.01408 + (-7.53949 + (278.69 + 52705.7 * (-0.98 + sigma)) * (-0.969 + sigma)) * (-0.9545 + sigma)) * (-0.99 + sigma)
    else:
        e = 0
    return e


def e_85(sigma: float) -> float:
    """
    Calculates efficiency for c_p = 85

    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """

    if (sigma >= 0.78) & (sigma <= 0.9630):
        e = 0.0255 + (-0.5 + (-2.44922 + (-10.8162 + (-106.848 + (-887.042 + (8089.1 + (217574.0 + (-6.24971 * (10 ** 6) + (2.20957 * (10 ** 8) + (4.97629 * (10 ** 9) + (5.70653 * (10 ** 10) + (1.01777 * (10 ** 12) + (-2.84954 * (10 ** 12) + (7.34495 * (10 ** 14) + (1.48198 * (10 ** 14) + (2.64076 * (10 ** 17) + (9.79507 * (10 ** 18) + (1.76652 * (10 ** 20) + 2.488 * (10 ** 21) * (-0.86 + sigma)) * (-0.91 + sigma)) * (-0.82 + sigma)) * (-0.9375 + sigma)) * (-0.85 + sigma)) * (-0.88 + sigma)) * (-0.83 + sigma)) * (-0.93 + sigma)) * (-0.8 + sigma)) * (-0.9545 + sigma)) * (-0.9 + sigma)) * (-0.79 + sigma)) * (-0.84 + sigma)) * (-0.946 + sigma)) * (-0.81 + sigma)) * (-0.92 + sigma)) * (-0.87 + sigma)) * (-0.78 + sigma)) * (-0.963 + sigma)
    elif (sigma > 0.9630) & (sigma <= 1):
        e = 0.0525 + (1.0 + (-5.88235 + 186.741 * (-0.98 + sigma)) * (-0.963 + sigma)) * (-0.99 + sigma)
    else:
        e = 0
    return e


def e_95(sigma: float) -> float:
    """
    Calculates efficiency for c_p = 95

    :param sigma: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
    :return: efficiency 1
    """

    if (sigma >= 0.78) & (sigma <= 0.9690):
        e = 0.0255 + (-0.531746 + (-2.62145 + (-16.8844 + (-139.19 + (-1080.24 + (-707.377 + (42113.5 + (-986834.0 + (-1.84915 * (10 ** 7) + (-9.99772 * (10 ** 8) + (-3.41909 * (10 ** 10) + (-1.21198 * (10 ** 12) + (6.73651 * (10 ** 13) + (6.28941 * (10 ** 14) + (2.90522 * (10 ** 16) + (4.12339 * (10 ** 17) + (-7.82365 * (10 ** 18) + (9.98686 * (10 ** 19) + (5.37003 * (10 ** 21) + 3.7992 * (10 ** 23) * (-0.91 + sigma)) * (-0.85 + sigma)) * (-0.9375 + sigma)) * (-0.82 + sigma)) * (-0.89 + sigma)) * (-0.86 + sigma)) * (-0.946 + sigma)) * (-0.83 + sigma)) * (-0.92 + sigma)) * (-0.8 + sigma)) * (-0.963 + sigma)) * (-0.9 + sigma)) * (-0.79 + sigma)) * (-0.84 + sigma)) * (-0.9545 + sigma)) * (-0.81 + sigma)) * (-0.93 + sigma)) * (-0.87 + sigma)) * (-0.78 + sigma)) * (-0.969 + sigma)
    elif (sigma > 0.9690) & (sigma <= 1):
        e = 0.044 + (0.880952 + 6.27706 * (-0.969 + sigma)) * (-0.99 + sigma)
    else:
        e = 0
    return e
