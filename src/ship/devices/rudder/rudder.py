from os import path
import src.ship.devices.rudder.e_functions_general as e_functions_general
from src.utils.convertions import *


class Rudder:
    """Class for the rudder description"""
    data_dir = path.normpath(r'data/ship/devices/rudder')

    def __init__(self, title: str = None, rudder_type: int = None, rudder_blade_area: float = None,
                 rudderpost_area: float = None, rudder_med_height: float = None) -> None:
        """
        Creates an instance of a rudder

        :param title: title of the rudder
        :param rudder_type: type of rudder 1 - 15 described in RMRS https://rs-class.org/
        :param rudder_blade_area: area of the rudder blade
        :param rudderpost_area: area of a rudder-post located within the height of the rudder
        :param rudder_med_height: the average height of the part of the rudder blade
                located aft of the axis of its rotation
        """

        self.title = title
        self.rudder_type = rudder_type

        self.rudder_blade_area = rudder_blade_area
        self.rudderpost_area = rudderpost_area
        self.rudder_med_height = rudder_med_height

    def __str__(self) -> str:
        """Representation in a string"""
        return f'{self.title}'

    def __repr__(self) -> str:
        """Full representation"""
        return f'Rudder({self.get_attributes()})'

    @property
    def title(self) -> str:
        """title of the rudder"""
        return self.__title

    @title.setter
    def title(self, value: str | None) -> None:
        if value:
            self.__title = str(value).strip()
        else:
            self.__title = None

    @property
    def rudder_type(self) -> int:
        """type of rudder 1 - 15 described in RMRS"""
        return self.__rudder_type

    @rudder_type.setter
    def rudder_type(self, value: str | int | None) -> None:
        rudder_type = string_to_int(value)

        if rudder_type in range(1, 16) or rudder_type is None:
            self.__rudder_type = rudder_type
        else:
            raise Exception('Rudder type is a type of rudder 1 - 15 described in RMRS https://rs-class.org/')

    @property
    def rudder_blade_area(self) -> float:
        """area of the rudder blade"""
        return self.__rudder_blade_area

    @rudder_blade_area.setter
    def rudder_blade_area(self, value: float | str | int | None) -> None:
        self.__rudder_blade_area = string_to_float(value)

    @property
    def rudderpost_area(self) -> float:
        """area of a rudder-post located within the height of the rudder"""
        return self.__rudderpost_area

    @rudderpost_area.setter
    def rudderpost_area(self, value: float | str | int | None) -> None:
        self.__rudderpost_area = string_to_float(value)

    @property
    def rudder_med_height(self) -> float:
        """the average height of the part of the rudder blade located aft of the axis of its rotation"""
        return self.__rudder_med_height

    @rudder_med_height.setter
    def rudder_med_height(self, value: float | str | int | None) -> None:
        self.__rudder_med_height = string_to_float(value)

    @property
    def rudder_area_oa(self) -> float:
        """area of the rudder blade or the rudder blade and the rudder-post depending on its construction"""
        if self.rudder_type not in {4, 10, 13}:
            return self.rudder_blade_area
        else:
            return self.rudderpost_area + self.rudder_blade_area

    @property
    def lamda_p(self) -> float:
        """ruder coefficient lambda p"""
        return round((self.rudder_med_height ** 2) / self.rudder_area_oa, 4)

    def get_attributes(self) -> dict:
        """
        Collects attributes from an instance

        :return: dict of attributes
        """

        attributes = {
            'title': self.title,
            'rudder_type': str(self.rudder_type),
            'dimensions': {
                'rudder_blade_area': str(self.rudder_blade_area),
                'rudderpost_area': str(self.rudderpost_area),
                'rudder_med_height': str(self.rudder_med_height)
            }
        }
        return attributes

    @classmethod
    def instantiate_from_attributes(cls, attributes: dict):
        """
        Creates an instance of a rudder from attributes

        :param attributes: dict of instance attributes
        """

        title = attributes['title']
        rudder_type = attributes['rudder_type']
        rudder_blade_area = attributes['dimensions']['rudder_blade_area']
        rudderpost_area = attributes['dimensions']['rudderpost_area']
        rudder_med_height = attributes['dimensions']['rudder_med_height']

        return cls(title=title, rudder_type=rudder_type, rudder_blade_area=rudder_blade_area,
                   rudderpost_area=rudderpost_area, rudder_med_height=rudder_med_height)

    @staticmethod
    def calculate_efficiency_1(cf_p: float, sigma_k: float) -> float:
        """
        Calculates efficiency 1 of ship rudders

        :param sigma_k: the coefficient of completeness of the underwater stern of the diametrical plane of the vessel
               at draft at summer load waterline
        :param cf_p: prismatic longitudinal coefficient
        :return: efficiency 1 of rudders
        """

        e_1 = e_functions_general.interpolate_e(c_p=cf_p, sigma=sigma_k)
        return e_1

    def calculate_efficiency_2(self, a_3: float, a_4: float, v: float, x_0: float, l_1: float) -> float:
        """
        Calculates efficiency 2 of ship rudders

        :param a_3: the area of the side sail of the vessel at such a minimum draft at which the
                    rudder blade or swivel nozzle is completely submerged in water
        :param a_4: the area of the underwater part of the diametrical plane of the ship with such a minimum
                    draft, in which the rudder blade or rotary nozzle is completely submerged in water
        :param v: service speed
        :param x_0: horizontal distance from midship frame (middle of length L) to center of gravity
                    area a_3, m. The value x_0 is taken positive when the center is located
                    gravity in the bow from the midship frame and negative - in the stern
        :param l_1: ship's length, measured at the level of the summer load line from the leading edge
                    stem to the extreme edge of the stern end of the vessel
        :return: efficiency 2 of rudders
        """

        e_2 = ((3.8 * a_3) / ((v ** 2) * a_4)) * (1 - 0.0667 * (a_3 / a_4)) * \
              (1 + (self.lamda_p - 1) * (0.33 + 0.015 * (v - 7.5)) - 5 * (x_0 / l_1))
        return round(e_2, 4)

    def calculate_efficiency_3(self, l_1: float, a_2: float, a_5: float, x: float) -> float:
        """
        Calculates efficiency 3 of ship rudders

        :param a_2: area of the underwater part of the center plane of the ship at summer draft
                    cargo waterline
        :param a_5: side sail area of a ship with summer load waterline draft
        :param l_1: ship's length, measured at the level of the summer load line from the leading edge
                    stem to the extreme edge of the stern end of the vessel
        :param x: horizontal distance from the midship frame (the middle of the length L1) to the center
                    the gravity of the area A_5, m. The value x is taken positive when the location
                    center of gravity in the bow from the midship frame and negative - in the stern
        :return: efficiency 3 of rudders, for vessels with L1 < 70m returns 0
        """

        if l_1 < 70:
            return 0
        else:
            e_3 = 0.03 + 0.01 * (self.lamda_p - 1) + 0.01 * (a_5 / a_2) * (1 - 3 * (x / l_1))
            return round(e_3, 4)
