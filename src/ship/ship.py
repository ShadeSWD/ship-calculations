from os import path
from src.utils.convertions import *


class Ship:
    """Class for the ship description"""
    data_dir = path.normpath(r'data/ship/ships')

    def __init__(self, title: str = None, ship_type: str = None, propellers_quantity: int = None,
                 velocity_design: float = None, x_of_rudder_shaft: float = None, hull=None, rudder=None):
        """
        Creates an instance of a ship

        :param title: title of the ship
        :param ship_type: type of vessel: cargo, tow, fishing
        :param propellers_quantity: quantity of propellers of the ship
        :param velocity_design: service speed of the vessel
        :param x_of_rudder_shaft: distance from midship to the rudder shaft positive when the shaft is located
               in the bow from the midship frame and negative - in the stern
        :param hull: instance of a hull - description and main dimensions
        :param rudder: instance of a rudder - description and main dimensions
        """

        self.title = title
        self.ship_type = ship_type
        self.propellers_quantity = propellers_quantity
        self.velocity_design = velocity_design
        self.x_of_rudder_shaft = x_of_rudder_shaft
        self.__hull = hull
        self.__rudder = rudder

    def __str__(self) -> str:
        """Representation in a string"""
        return f'{self.__title}'

    def __repr__(self) -> str:
        """Full representation"""
        return f'Ship({self.get_attributes()})'

    @property
    def title(self) -> str:
        """title of the ship"""
        return self.__title

    @title.setter
    def title(self, value: str | None) -> None:
        if value:
            self.__title = str(value).strip()
        else:
            self.__title = None

    @property
    def ship_type(self) -> str:
        """type of vessel: cargo, tow, fishing"""
        return self.__ship_type

    @ship_type.setter
    def ship_type(self, value: str | None) -> None:
        if value:
            self.__ship_type = str(value).strip()
        else:
            self.__ship_type = None

    @property
    def propellers_quantity(self) -> int:
        """quantity of propellers of the ship"""
        return self.__propellers_quantity

    @propellers_quantity.setter
    def propellers_quantity(self, value: float | str | int | None) -> None:
        propellers_quantity = string_to_int(value)
        if propellers_quantity is None:
            self.__propellers_quantity = propellers_quantity
        elif propellers_quantity >= 0:
            self.__propellers_quantity = propellers_quantity
        else:
            raise Exception('Propellers quantity must be None or integer')

    @property
    def velocity_design(self) -> float:
        """service speed of the vessel"""
        return self.__velocity_design

    @velocity_design.setter
    def velocity_design(self, value: float | str | int | None) -> None:
        self.__velocity_design = string_to_float(value)

    @property
    def x_of_rudder_shaft(self) -> float:
        """distance from midship to the rudder shaft positive when the shaft is located
               in the bow from the midship frame and negative - in the stern"""
        return self.__x_of_rudder_shaft

    @x_of_rudder_shaft.setter
    def x_of_rudder_shaft(self, value: float | str | int | None) -> None:
        self.__x_of_rudder_shaft = string_to_float(value)

    @property
    def sigma_k(self) -> float:
        """coefficient of completeness of the underwater stern part of the diametrical plane of the vessel at
            draft at summer load waterline"""
        f = self.hull.area_stern_valance_summer_wl
        f_0 = self.hull.area_blades_on_stern_valance_summer_wl
        return round(1 - 2 * (f - f_0) / (self.hull.length_summer_wl * self.hull.draught_summer_wl), 4)

    @property
    def hull(self):
        """instance of a hull - description and main dimensions"""
        return self.__hull

    @hull.setter
    def hull(self, value):
        self.__hull = value

    @property
    def rudder(self):
        """instance of a rudder - description and main dimensions"""
        return self.__rudder

    @rudder.setter
    def rudder(self, value):
        self.__rudder = value

    def calculate_rudder_efficiencies(self) -> dict:
        """
        Calculates efficiencies of the rudder
        :return: {'e1': e1, 'e2': e2, 'e3': e3}
        """

        e1 = self.rudder.calculate_efficiency_1(cf_p=self.hull.cf_p_summer_wl, sigma_k=self.sigma_k)
        e2 = self.rudder.calculate_efficiency_2(a_3=self.hull.area_side_sail_draught_min_rudder,
                                                a_4=self.hull.area_diam_butt_draught_min_rudder,
                                                v=self.velocity_design,
                                                x_0=self.hull.x_g_area_side_sail_draught_min_rudder,
                                                l_1=self.hull.length_summer_wl)
        e3 = self.rudder.calculate_efficiency_3(a_2=self.hull.area_diam_butt_summer_wl,
                                                a_5=self.hull.area_side_sail_draught_summer_wl,
                                                l_1=self.hull.length_summer_wl,
                                                x=self.hull.x_g_area_side_sail_draught_summer_wl)
        efficiencies = {'e1': e1, 'e2': e2, 'e3': e3}
        return efficiencies

    def get_attributes(self) -> dict:
        """
        Collects attributes from an instance
        :return: dict of attributes
        """

        attributes = {
            'title': self.title,
            'ship_type': self.ship_type,
            'propellers_quantity': self.propellers_quantity,
            'velocity_design': self.velocity_design,
            'x_of_rudder_shaft': self.x_of_rudder_shaft,
            'sub_parts': {
                'hull': str(self.hull),
                'rudder': str(self.rudder)
            },
        }
        return attributes

    @classmethod
    def instantiate_from_attributes(cls, attributes: dict):
        """
        Creates an instance of a ship from attributes
        :param attributes: dict of instance attributes
        """

        title = attributes['title']
        propellers_quantity = attributes['propellers_quantity']
        ship_type = attributes['ship_type']
        velocity_design = attributes['velocity_design']
        x_of_rudder_shaft = attributes['x_of_rudder_shaft']
        hull = attributes['sub_parts']['hull']
        rudder = attributes['sub_parts']['rudder']

        return cls(title=title, ship_type=ship_type, propellers_quantity=propellers_quantity,
                   velocity_design=velocity_design, x_of_rudder_shaft=x_of_rudder_shaft, hull=hull, rudder=rudder)
