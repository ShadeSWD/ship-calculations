from src.utils.ship_maths import calculate_distance_pts, calculate_angle_pts, interpolate, calculate_polygon_square, \
    calculate_x_0
from src.utils.geometry_units import Point


class Buttock:
    """Model of a buttock"""

    def __init__(self, y: float | int, points: list) -> None:
        """
        Creates instance of a buttock
        :param y: coordinate y
        :param points: points of a buttock
        """

        self.y = y
        self.points = points

    def __str__(self):
        """Representation in a string"""
        return f'butt({self.y})'

    def __repr__(self):
        """Full representation"""
        return f'Buttock(y={self.y}, points={", ".join(str(pt) for pt in self.points)})'

    @property
    def points(self) -> list:
        """Returns points of a frame"""
        return self.__points

    @points.setter
    def points(self, value: list):
        points = value
        sorted_cords = self.sort_buttock(points)
        self.__points = sorted_cords

    @staticmethod
    def collect_point_distances(mixed_coords: list) -> list:
        """
        Calculates distances between all points in the list and returns a list with distances

        :param mixed_coords: list of Points
        :return: list of Points
        """

        points_distances = []

        try:
            for pt_1 in mixed_coords:
                for pt_2 in mixed_coords:
                    dist = calculate_distance_pts(x1=pt_1.x, y1=pt_1.z, x2=pt_2.x, y2=pt_2.z)
                    data = {'pt_1': pt_1, 'pt_2': pt_2,
                            'dist': dist}
                    points_distances.append(data)
        except IndexError:
            pass
        return points_distances

    @staticmethod
    def get_dist_btw_pts(points_distances: list, pt_1: dict, pt_2: dict) -> float:
        """
        :param points_distances: list of Points
        :param pt_1: first point to find distance
        :param pt_2: second point
        :return: distance between points if found
        """

        for line in points_distances:
            if line['pt_1'] == pt_1 and line['pt_2'] == pt_2:
                return line['dist']
        raise Exception(f'distance does not exist, {pt_1}, {pt_2}\n{points_distances}')

    def sort_buttock(self, mixed_coords: list) -> list:
        """
        Sorts points into polygon using distance between points and angles between neighbour lines

        :param mixed_coords: list of Points
        :return: list of Points
        """
        angle_weight = 0.021

        points_distances = self.collect_point_distances(mixed_coords=mixed_coords)
        sorted_coords = []

        start_point = mixed_coords[0]
        sorted_coords.append(start_point)
        second_point = None

        dist_to_start = 1000
        for pt in mixed_coords:
            dist = self.get_dist_btw_pts(points_distances=points_distances, pt_1=start_point, pt_2=pt)
            if dist < dist_to_start and dist != 0:
                dist_to_start = dist
                second_point = pt
        sorted_coords.append(second_point)
        while len(sorted_coords) < len(mixed_coords):
            points_values = []
            for pt in mixed_coords:
                if pt not in sorted_coords:
                    angle = calculate_angle_pts(x1=sorted_coords[-2].x, y1=sorted_coords[-2].z, x2=sorted_coords[-1].x,
                                                y2=sorted_coords[-1].z, x3=pt.x, y3=pt.z)

                    dist_btw_pts = self.get_dist_btw_pts(points_distances, pt, sorted_coords[-1])
                    value = dist_btw_pts + angle_weight * angle
                    data = {'point': pt, angle: 'angle', 'dist_btw_pts': dist_btw_pts, 'value': value}
                    points_values.append(data)
            next_point = None
            min_value = 1000
            for point in points_values:
                pt_value = point['value']
                if pt_value < min_value:
                    min_value = pt_value
                    next_point = point['point']
            sorted_coords.append(next_point)
        return sorted_coords

    def get_x_0_under_water(self, level: float) -> float:
        """
        Gets x of the center of the polygon
        :param level: draft
        :return: x_c
        """

        coords = self.get_underwater_diam_butt_coords(level=level)
        polygon_coords = [[coord.x, coord.z] for coord in coords]
        x_c = calculate_x_0(polygon_coords=polygon_coords, z_max=level)
        return x_c

    def get_x_0_upon_water(self, level: float) -> float:
        """
        Gets x of the center of the polygon
        :param level: draft
        :return: x_c
        """

        highest_pt = 0
        for pt in self.points:
            if pt.z > highest_pt:
                highest_pt = pt.z
        coords = self.get_underwater_diam_butt_coords(level=highest_pt)
        polygon_coords = [[coord.x, coord.z] for coord in coords]
        x_c = calculate_x_0(polygon_coords=polygon_coords, z_max=level)
        return x_c

    def get_upon_water_diam_butt_coords(self, level: float) -> list:
        """
        Gets points of the on surface part of diametrical buttock
        :param level: draft
        :return: list of points
        """

        underwater_coords = self.get_underwater_diam_butt_coords(level=level)
        first_pt = underwater_coords[0]
        last_pt = underwater_coords[-1]

        on_surface_diam_butt_coords = [first_pt, last_pt]
        for coord in self.points:
            if coord not in underwater_coords:
                on_surface_diam_butt_coords.append(coord)

        return on_surface_diam_butt_coords

    def get_underwater_diam_butt_coords(self, level: float) -> list:
        """
        Gets points of the underwater part of diametrical buttock
        :param level: draft
        :return: list of points
        """

        underwater_diam_butt_coords = []
        first_pt = None
        first_pt_index = None
        last_pt = None
        last_pt_index = None
        i = 0
        while i < len(self.points):
            if self.points[i].z <= level:
                first_pt = self.points[i]
                first_pt_index = i
                break
            i += 1
        i = first_pt_index + 1
        while i < len(self.points):
            if self.points[i].z >= level:
                last_pt = self.points[i]
                last_pt_index = i
                break
            i += 1

        first_pt_x = interpolate(x1=first_pt.z, y1=first_pt.x, x2=self.points[first_pt_index - 1].z,
                                 y2=self.points[first_pt_index - 1].x, x3=level)
        last_pt_x = interpolate(x1=last_pt.z, y1=last_pt.x, x2=self.points[last_pt_index - 1].z,
                                y2=self.points[last_pt_index - 1].x, x3=level)
        first_pt_int = Point(x=first_pt_x, y=self.y, z=level)
        last_pt_int = Point(x=last_pt_x, y=self.y, z=level)
        underwater_diam_butt_coords.append(first_pt_int)
        underwater_diam_butt_coords.extend(self.points[first_pt_index: last_pt_index])
        underwater_diam_butt_coords.append(last_pt_int)

        return underwater_diam_butt_coords

    def calculate_area_under_water(self, level: float) -> float:
        """
        Calculates area of a buttock at the desired level
        :param level: draft under which coordinates should be found
        :return: area of a frame
        """

        buttock_coords = self.get_underwater_diam_butt_coords(level=level)
        polygon_coords = [[pt.x, pt.z] for pt in buttock_coords]
        area = calculate_polygon_square(polygon_coords=polygon_coords)
        area = round(area * 2, 4)
        return area

    def calculate_area_upon_water(self, level: float) -> float:
        """
        Calculates area of a buttock at the desired draft
        :param level: level upon which coordinates should be found
        :return: area of a buttock
        """

        buttock_coords = self.get_upon_water_diam_butt_coords(level=level)
        polygon_coords = [[pt.x, pt.z] for pt in buttock_coords]
        area = calculate_polygon_square(polygon_coords=polygon_coords)
        area = round(area * 2, 4)
        return area
