from os import path
from src.utils.geometry_units import Point
from src.utils.ship_maths import interpolate, calculate_polygon_square
from src.ship.hull.frame_s2g import Frame
from src.ship.hull.buttock_s2g import Buttock


class HullFromS2G:
    """Class for the s2g hull description"""

    data_dir = path.normpath(r'data/ship/hull/s2g_hulls')

    def __init__(self, filename: str, title: str = None, draught_design: float = None,
                 draught_summer_wl: float = None, ) -> None:
        """Creates an instance of a hull

        :param filename: name of the s2g file located in ...data/ship/hull/s2g_hulls
        :param title: title of the hull
        :param draught_design: construction draught
        :param draught_summer_wl: to summer waterline
        """

        self.filename = filename
        self.title = title

        self.draught_design = draught_design
        self.draught_summer_wl = draught_summer_wl

        self.__hull_data = self.get_hull_data()
        self.__diam_buttock = self.get_diam_buttock()

    def __str__(self) -> str:
        """Representation in a string"""
        return f'{self.__title} from s2g'

    def __repr__(self) -> str:
        """Full representation"""
        return f'HullFromS2G({self.get_attributes()})'

    @property
    def filename(self) -> str:
        """name of the s2g file located in ...data/ship/hull/s2g_hulls"""
        return self.__filename

    @filename.setter
    def filename(self, value: str) -> None:
        if '.s2g' in value:
            self.__filename = value
        else:
            raise Exception('Invalid file format')

    @property
    def title(self) -> str:
        """title of the hull"""
        return self.__title

    @title.setter
    def title(self, value: str | None) -> None:
        if value:
            self.__title = str(value).strip()
        else:
            self.__title = self.filename.strip('.s2g') + '_s2g'

    @property
    def hull_data(self) -> list:
        """Hull data - list of frames"""
        return self.__hull_data

    @property
    def diam_buttock(self):
        """list of sorted diametrical buttock coordinates"""
        return self.__diam_buttock

    @property
    def breadth_oa(self) -> float:
        """Maximum breadth of the vessel"""
        max_y = 0
        for frame in self.hull_data:
            for pt in frame.points:
                if pt.y > max_y:
                    max_y = pt.y
        breadth = max_y * 2
        return breadth

    @property
    def length_oa(self) -> float:
        """Maximum length of the vessel"""
        length = - self.hull_data[0].x + self.hull_data[-1].x
        return round(length, 4)

    @property
    def x_middle(self) -> float:
        """Finds x of the frame closest to the middle"""
        x_min = abs(self.hull_data[0].x)
        for frame in self.hull_data:
            if abs(frame.x) < abs(x_min):
                x_min = frame.x
        return x_min

    def diam_buttock_x_0_under_water(self, level: float | int) -> float:
        """Gets x_0 of diametrical buttock under the water
        :param level: draft
        :return: x_0
        """
        return round(self.diam_buttock.get_x_0_under_water(level=level), 4)

    def diam_buttock_x_0_upon_water(self, level: float | int) -> float:
        """Gets x_0 of diametrical buttock upon the water
        :param level: draft
        :return: x_0
        """
        return round(self.diam_buttock.get_x_0_upon_water(level=level), 4)

    def draught_min_rudder(self, x_of_rudder_shaft: int | float) -> float:
        """
        A minimum draft, in which the rudder blade or rotary nozzle is completely submerged in water
        :param x_of_rudder_shaft: position of rudder shaft
        :return: draught
        """

        lfr = None
        rfr = None
        for frame in self.hull_data:
            if frame.x <= x_of_rudder_shaft:
                lfr = frame
            else:
                rfr = frame

        xl = lfr.x
        zl = 0
        xr = rfr.x
        zr = 0
        for pt in lfr.points:
            if pt.y == 0 and pt.z < self.draught_summer_wl:
                zl = pt.z
        for pt in rfr.points:
            if pt.y == 0 and pt.z < self.draught_summer_wl:
                zr = pt.z

        draught = interpolate(x1=xr, y1=zr, x2=xl, y2=zl, x3=x_of_rudder_shaft)
        return draught

    def calculate_volume(self, level: int | float) -> float:
        """
        Calculates volume of the vessel
        :param level: draft
        :return: volume
        """

        frame_areas = [[0, 0]]
        frame_areas.extend([[frame.x, frame.calculate_area_under_water(level=level)] for frame in self.hull_data])
        frame_areas.append([frame_areas[-1][0], 0])

        volume = round(calculate_polygon_square(frame_areas), 4)

        return volume

    def calculate_diam_buttock_area_under_water(self, level: int | float) -> float:
        """
        Calculates area of diametrical buttock under the water
        :param level: draught
        :return: area
        """

        area = self.diam_buttock.calculate_area_under_water(level=level)
        return area

    def calculate_diam_buttock_area_upon_water(self, level: int | float) -> float:
        """
        Calculates area of diametrical buttock upon the water
        :param level: draught
        :return: area
        """

        area = self.diam_buttock.calculate_area_upon_water(level=level)
        return area

    def calculate_frame_area(self, x: int | float, level: int | float) -> float:
        """
        Calculates breadth of diametrical buttock
        :param x: x
        :param level: draught
        :return: area
        """

        area = 0
        for frame in self.hull_data:
            if frame.x == x:
                area = frame.calculate_area_under_water(level=level)
        return area

    def calculate_breadth(self, level: int | float) -> float:
        """
        Calculates breadth at the desired level
        :param level: draught
        :return: length of the waterline
        """

        max_y = 0
        for frame in self.hull_data:
            frm_pts = frame.get_coords_under_water(level=level)
            for pt in frm_pts:
                if pt.y > max_y:
                    max_y = pt.y
        breadth = max_y * 2
        return breadth

    def calculate_length(self, level: int | float) -> float:
        """
        Calculates length at the desired level
        :param level: draught
        :return: length of the waterline
        """

        first_pt = None
        first_pt_index = None
        last_pt = None
        last_pt_index = None
        i = 0
        diam_butt_coords = self.diam_buttock.points
        while i < len(diam_butt_coords):
            if diam_butt_coords[i].z <= level:
                first_pt = diam_butt_coords[i]
                first_pt_index = i
                break
            i += 1
        i = first_pt_index + 1
        while i < len(diam_butt_coords):
            if diam_butt_coords[i].z >= level:
                last_pt = diam_butt_coords[i]
                last_pt_index = i
                break
            i += 1
        first_pt_x = interpolate(x1=first_pt.z, y1=first_pt.x, x2=diam_butt_coords[first_pt_index - 1].z,
                                 y2=diam_butt_coords[first_pt_index - 1].x, x3=level)
        last_pt_x = interpolate(x1=last_pt.z, y1=last_pt.x, x2=diam_butt_coords[last_pt_index - 1].z,
                                y2=diam_butt_coords[last_pt_index - 1].x, x3=level)
        length = abs(first_pt_x - last_pt_x)
        return length

    def get_hull_data(self) -> list:
        """Collects data from s2g file

        :return: Hull data in format made of frames with points
        """

        with open(file=path.normpath(self.data_dir + r'/' + self.filename), mode='rt') as file:
            ship_data = []
            imported_file = file.readlines()
            i = 0
            while i < len(imported_file):
                try:
                    spl_line = imported_file[i].split()
                    if spl_line[0] == '&N':
                        j = i
                        n_fr = int(spl_line[1])
                        j += 1
                        spl_line = imported_file[j].split()
                        if spl_line[0] == '&X':
                            x_fr = float(spl_line[1])
                            j += 1
                            spl_line = imported_file[j].split()
                            if spl_line[0] == '&DN':
                                dn_fr = float(spl_line[1])
                                j += 1
                                spl_line = imported_file[j].split()
                                points = []
                                while spl_line[0] == '&YZ':
                                    point = Point(x=x_fr, y=float(spl_line[1]), z=float(spl_line[2]))
                                    points.append(point)
                                    j += 1
                                    try:
                                        spl_line = imported_file[j].split()
                                    except IndexError:
                                        break
                    i = j
                    try:
                        frame = Frame(x=x_fr, n=n_fr, dn=dn_fr, points=points)
                        ship_data.append(frame)
                    except KeyError:
                        pass
                except IndexError:
                    i += 1
        ship_data.sort(key=lambda frm: frm.x)
        return ship_data

    def get_diam_buttock(self):
        """
        Creates an instance of diametrical buttock

        :return: instance of a buttock
        """

        diam_butt_coords = []

        for frame in self.hull_data:
            try:
                for pt in frame.points:
                    if pt.y == 0:
                        diam_butt_coords.append(pt)
            except KeyError:
                pass

        diam_buttock = Buttock(y=0, points=diam_butt_coords)

        return diam_buttock

    def get_frame_coords_x(self, x_fr: float) -> list:
        """
        Gets full frame coordinates at a certain x_fr (existed in the original s2g)

        :param x_fr: x of the frame existing in s2g file
        :return: list of coordinates of the frame
        """

        frame_coords = []
        for frame in self.hull_data:
            try:
                if frame.x == x_fr:
                    for pt in frame.points:
                        frame_coords.append(pt)
            except KeyError:
                pass
        return frame_coords

    def calculate_stern_valance_area(self, level: float | int) -> float:
        """
        Area of stern valance
        :param level: draft
        :return: area
        """

        stern_valance_coords = self.get_stern_valance(level=level)
        polygon_coords = [[pt.x, pt.z] for pt in stern_valance_coords]
        area = calculate_polygon_square(polygon_coords=polygon_coords)
        area = round(area, 4)
        return area

    def get_stern_valance(self, level: float | int) -> list:
        """
        area of the lateral projection of the vessel's stern valance, m2,
        calculated as area a figure bounded by a continuation line of the lower edge of the keel, a
        perpendicular, lowered onto this line from the point of intersection of the summer load waterline
        with the contour of the diametrical section of the stern end of the vessel, and the outer
        the edge of the stern post, drawn without taking into account the rudder post, the sole of the stern post
        or steering wheel bracket, if any
        :param level: draft
        :return: coordinates of the stern valance
        """

        diam_butt_coords = self.diam_buttock.points
        x_min = 0
        first_pt = None
        first_pt_index = None
        i = 0
        while i < len(diam_butt_coords):
            if diam_butt_coords[i].x < x_min and diam_butt_coords[i].z <= level:
                x_min = diam_butt_coords[i].x
                z_min = diam_butt_coords[i].z
                first_pt = diam_butt_coords[i]
                first_pt_index = i
                j = i + 1
                while diam_butt_coords[i].x == diam_butt_coords[j].x:
                    try:
                        if diam_butt_coords[j].z < z_min:
                            first_pt = diam_butt_coords[i + 1]
                            first_pt_index = j
                            j += 1
                    except IndexError:
                        pass
                i = j
            else:
                i += 1
        first_pt_x = interpolate(x1=first_pt.z, y1=first_pt.x, x2=diam_butt_coords[first_pt_index - 1].z,
                                 y2=diam_butt_coords[first_pt_index - 1].x, x3=level)
        if first_pt_x != first_pt.x:
            first_pt = Point(x=first_pt_x, y=0, z=level)
        else:
            first_pt_index += 1

        last_pt = None
        last_pt_index = None
        i = first_pt_index
        while i < len(diam_butt_coords):
            if diam_butt_coords[i].z != 0:
                i += 1
            else:
                last_pt = diam_butt_coords[i]
                last_pt_index = i
                break

        extra_point = Point(x=first_pt.x, y=0, z=last_pt.z)

        stern_valance_coords = [first_pt]
        stern_valance_coords.extend(diam_butt_coords[first_pt_index: last_pt_index + 1])
        stern_valance_coords.append(extra_point)

        return stern_valance_coords

    @staticmethod
    def get_area_blades_on_stern_valance_summer_wl(propellers_quantity: int) -> float:
        """
        For twin-screw ships - the area of the lateral projection of the propeller fairings (or part of it)
        superimposed on the area of the figure
        :param propellers_quantity: number of propellers
        :return: area of blade covers
        """

        area = None
        if propellers_quantity != 2:
            area = 0
        return area

    @classmethod
    def instantiate_from_attributes(cls, attributes: dict):
        """
        Creates an instance of a hull from attributes

        :param attributes: dict of instance attributes
        """

        filename = attributes['filename']
        title = attributes['title']
        draught_design = attributes['dimensions']['draught_design']
        draught_summer_wl = attributes['dimensions']['draught_summer_wl']

        return cls(filename=filename, title=title, draught_design=draught_design, draught_summer_wl=draught_summer_wl)

    def get_attributes(self) -> dict:
        """
        Collects attributes from an instance

        :return: dict of attributes
        """

        attributes = {
            'filename': self.filename,
            'title': self.title,
            'dimensions': {
                'draught_design': str(self.draught_design),
                'draught_summer_wl': str(self.draught_summer_wl),
            }
        }

        return attributes
