from src.utils.geometry_units import Point
from src.utils.ship_maths import interpolate, calculate_polygon_square


class Frame:
    """Model of a frame"""

    def __init__(self, x: float | int, n: float | int, dn: float | int, points: list) -> None:
        """
        Creates an instance of a frame
        :param x: coordinate x
        :param n: special number
        :param points: points of a frame
        """
        self.x = x
        self.n = n
        self.dn = dn
        self.points = points

    def __str__(self):
        return f'frm({self.x})'

    def __repr__(self):
        return f'Frame(x={self.x}, n={self.n}, dn={self.dn}, points={", ".join(str(pt) for pt in self.points)})'

    @property
    def points(self) -> list:
        """Returns points of a frame"""
        return self.__points

    @points.setter
    def points(self, value: list):
        points = value
        points.sort(key=lambda pt: pt.z)
        self.__points = points

    def calculate_area_under_water(self, level: float) -> float:
        """
        Calculates area of a frame at the desired draft
        :param level: level under which coordinates should be found
        :return: area of a frame
        """

        frame_coords = self.get_coords_under_water(level=level)
        polygon_coords = [[pt.y, pt.z] for pt in frame_coords]
        half_area = calculate_polygon_square(polygon_coords=polygon_coords)
        area = round(half_area * 2, 4)
        return area

    def get_coords_under_water(self, level: float) -> list:
        """
        Gets frame coordinates at a certain level

        :param level: level under which coordinates should be found
        :return: list of coordinates of the frame
        """

        frame_coords = []
        extra_pt = None
        flag = False

        for pt in self.points:
            if pt.z <= level:
                frame_coords.append(pt)
            elif not flag:
                flag = True
                extra_pt = pt

        try:
            last_point_y = interpolate(x1=frame_coords[-1].z, y1=frame_coords[-1].y, x2=extra_pt.z,
                                       y2=extra_pt.y, x3=level)
            last_point = Point(x=frame_coords[0].x, y=last_point_y, z=level)
            if frame_coords[-1] != last_point:
                frame_coords.append(last_point)
            frame_coords.append(Point(x=frame_coords[0].x, y=0, z=level))
        except IndexError:
            pass
        return frame_coords
