from os import path
from src.utils.convertions import *
from src.ship.hull.hull_s2g import HullFromS2G


class Hull:
    """Class for the hull description"""
    data_dir = path.normpath(r'data/ship/hull')

    def __init__(self, title: str = None, hull_source: str = 'manual', length_oa: float = None, length_pp: float = None,
                 length_wl: float = None, length_summer_wl: float = None, breadth_oa: float = None,
                 breadth_design: float = None, breadth_summer_wl: float = None, draught_design: float = None,
                 draught_summer_wl: float = None, draught_min_rudder: float = None,
                 area_diam_butt_summer_wl: float = None, area_diam_butt_draught_min_rudder: float = None,
                 area_midship_design: float = None, area_midship_summer_wl: float = None,
                 area_side_sail_draught_min_rudder: float = None, area_side_sail_draught_summer_wl: float = None,
                 volume_design: float = None, volume_summer_wl: float = None,
                 x_g_area_side_sail_draught_min_rudder: float = None,
                 x_g_area_side_sail_draught_summer_wl: float = None, area_stern_valance_summer_wl: float = None,
                 area_blades_on_stern_valance_summer_wl: float = None) -> None:
        """
        Creates an instance of a hull

        :param title: title of the hull
        :param hull_source: specify file or direct input (manual)
        :param length_oa: the maximum length of the vessel
        :param length_pp: length between bow and stern perpendiculars
        :param length_wl: length of construction waterline
        :param length_summer_wl: length of the summer load waterline
        :param breadth_oa: the maximum breadth of the vessel
        :param breadth_design: breadth at the construction waterline
        :param breadth_summer_wl: breadth at the summer waterline
        :param draught_design: construction draught
        :param draught_summer_wl: draught to summer waterline
        :param draught_min_rudder: a minimum draft, in which the rudder blade or rotary nozzle
               is completely submerged in water
        :param area_diam_butt_summer_wl: area of the underwater stern of the diametrical plane of the vessel
               at draft at summer load waterline
        :param area_diam_butt_draught_min_rudder: the area of the underwater part of the diametrical plane of the ship
               with such a minimum draft, in which the rudder blade or rotary nozzle is completely submerged in water
        :param area_midship_design: area of the underwater part of midship of the vessel at design draft
        :param area_midship_summer_wl: area of the underwater part of midship of the vessel at draft at
               summer load waterline
        :param area_side_sail_draught_min_rudder: the area of the side sail of the vessel at such a minimum draft at
               which the rudder blade or swivel nozzle is completely submerged in water
        :param area_side_sail_draught_summer_wl: side sail area of a ship with summer load waterline draft
        :param volume_design: volume at the construction waterline
        :param volume_summer_wl: volume at the summer load waterline
        :param x_g_area_side_sail_draught_min_rudder: horizontal distance from midship frame (middle of length L) to
               center of gravity area a_3, m. The value x_0 is taken positive when the gravity center is located
               in the bow from the midship frame and negative - in the stern
        :param x_g_area_side_sail_draught_summer_wl: horizontal distance from the midship frame (the middle of the
               length L1) to the center the gravity of the area A_5, m. The value x is taken positive when the location
               center of gravity in the bow from the midship frame and negative - in the stern
        :param area_stern_valance_summer_wl: area of the lateral projection of the vessel's stern valance, m2,
               calculated as area a figure bounded by a continuation line of the lower edge of the keel, a
               perpendicular, lowered onto this line from the point of intersection of the summer load waterline
               with the contour of the diametrical section of the stern end of the vessel, and the outer
               the edge of the stern post, drawn without taking into account the rudder post, the sole of the stern post
               or steering wheel bracket, if any;
        :param area_blades_on_stern_valance_summer_wl: for twin-screw ships - the area of the lateral projection of the
               propeller fairings (or part of it) superimposed on the area of the figure
        """

        self.title = title
        self.hull_source = hull_source

        self.length_oa = length_oa
        self.length_pp = length_pp
        self.length_wl = length_wl
        self.length_summer_wl = length_summer_wl

        self.breadth_oa = breadth_oa
        self.breadth_design = breadth_design
        self.breadth_summer_wl = breadth_summer_wl

        self.draught_design = draught_design
        self.draught_summer_wl = draught_summer_wl
        self.draught_min_rudder = draught_min_rudder

        self.area_diam_butt_summer_wl = area_diam_butt_summer_wl
        self.area_diam_butt_draught_min_rudder = area_diam_butt_draught_min_rudder
        self.area_midship_design = area_midship_design
        self.area_midship_summer_wl = area_midship_summer_wl
        self.area_side_sail_draught_min_rudder = area_side_sail_draught_min_rudder
        self.area_side_sail_draught_summer_wl = area_side_sail_draught_summer_wl

        self.volume_design = volume_design
        self.volume_summer_wl = volume_summer_wl

        self.x_g_area_side_sail_draught_min_rudder = x_g_area_side_sail_draught_min_rudder
        self.x_g_area_side_sail_draught_summer_wl = x_g_area_side_sail_draught_summer_wl

        self.area_stern_valance_summer_wl = area_stern_valance_summer_wl
        self.area_blades_on_stern_valance_summer_wl = area_blades_on_stern_valance_summer_wl

    def __str__(self) -> str:
        """Representation in a string"""
        return f'{self.__title}'

    def __repr__(self) -> str:
        """Full representation"""
        return f'Hull({self.get_attributes()})'

    @property
    def title(self) -> str:
        """title of the hull"""
        return self.__title

    @title.setter
    def title(self, value: str | None) -> None:
        if value:
            self.__title = str(value).strip()
        else:
            self.__title = None

    @property
    def length_oa(self) -> float:
        """the maximum length of the vessel"""
        return self.__length_oa

    @length_oa.setter
    def length_oa(self, value: float | str | int | None) -> None:
        self.__length_oa = string_to_float(value)

    @property
    def length_pp(self) -> float:
        """length between bow and stern perpendiculars"""
        return self.__length_pp

    @length_pp.setter
    def length_pp(self, value: float | str | int | None) -> None:
        self.__length_pp = string_to_float(value)

    @property
    def length_wl(self) -> float:
        """length of construction waterline"""
        return self.__length_wl

    @length_wl.setter
    def length_wl(self, value: float | str | int | None) -> None:
        self.__length_wl = string_to_float(value)

    @property
    def length_summer_wl(self) -> float:
        """length of the summer load waterline"""
        return self.__length_summer_wl

    @length_summer_wl.setter
    def length_summer_wl(self, value: float | str | int | None) -> None:
        self.__length_summer_wl = string_to_float(value)

    @property
    def breadth_oa(self):
        """the maximum breadth of the vessel"""
        return self.__breadth_oa

    @breadth_oa.setter
    def breadth_oa(self, value: float | str | int | None) -> None:
        self.__breadth_oa = string_to_float(value)

    @property
    def breadth_design(self):
        """breadth at the construction waterline"""
        return self.__breadth_design

    @breadth_design.setter
    def breadth_design(self, value: float | str | int | None) -> None:
        self.__breadth_design = string_to_float(value)

    @property
    def breadth_summer_wl(self):
        """breadth at the summer waterline"""
        return self.__breadth_summer_wl

    @breadth_summer_wl.setter
    def breadth_summer_wl(self, value: float | str | int | None) -> None:
        self.__breadth_summer_wl = string_to_float(value)

    @property
    def draught_design(self) -> float:
        """construction draught"""
        return self.__draught_design

    @draught_design.setter
    def draught_design(self, value: float | str | int | None) -> None:
        self.__draught_design = string_to_float(value)

    @property
    def draught_summer_wl(self) -> float:
        """draught to summer waterline"""
        return self.__draught_summer_wl

    @draught_summer_wl.setter
    def draught_summer_wl(self, value: float | str | int | None) -> None:
        self.__draught_summer_wl = string_to_float(value)

    @property
    def draught_min_rudder(self) -> float:
        """a minimum draft, in which the rudder blade or rotary nozzle is completely submerged in water"""
        return self.__draught_min_rudder

    @draught_min_rudder.setter
    def draught_min_rudder(self, value: float | str | int | None) -> None:
        self.__draught_min_rudder = string_to_float(value)

    @property
    def area_diam_butt_summer_wl(self) -> float:
        """area of the underwater stern of the diametrical plane of the vessel
            at draft at summer load waterline"""
        return self.__area_diam_butt_summer_wl

    @area_diam_butt_summer_wl.setter
    def area_diam_butt_summer_wl(self, value: float | str | int | None) -> None:
        self.__area_diam_butt_summer_wl = string_to_float(value)

    @property
    def area_diam_butt_draught_min_rudder(self) -> float:
        """the area of the underwater part of the diametrical plane of the ship with such a minimum draft,
        in which the rudder blade or rotary nozzle is completely submerged in water"""
        return self.__area_diam_butt_draught_min_rudder

    @area_diam_butt_draught_min_rudder.setter
    def area_diam_butt_draught_min_rudder(self, value: float | str | int | None) -> None:
        self.__area_diam_butt_draught_min_rudder = string_to_float(value)

    @property
    def area_midship_design(self) -> float:
        """area of the underwater part of midship of the vessel at design draft"""
        return self.__area_midship_design

    @area_midship_design.setter
    def area_midship_design(self, value: float | str | int | None) -> None:
        self.__area_midship_design = string_to_float(value)

    @property
    def area_midship_summer_wl(self) -> float:
        """area of the underwater part of midship of the vessel at draft at
           summer load waterline"""
        return self.__area_midship_summer_wl

    @area_midship_summer_wl.setter
    def area_midship_summer_wl(self, value: float | str | int | None) -> None:
        self.__area_midship_summer_wl = string_to_float(value)

    @property
    def area_side_sail_draught_min_rudder(self) -> float:
        """area_side_sail_draught_min_rudder: the area of the side sail of the vessel at such a minimum draft at
               which the rudder blade or swivel nozzle is completely submerged in water"""
        return self.__area_side_sail_draught_min_rudder

    @area_side_sail_draught_min_rudder.setter
    def area_side_sail_draught_min_rudder(self, value: float | str | int | None) -> None:
        self.__area_side_sail_draught_min_rudder = string_to_float(value)

    @property
    def area_side_sail_draught_summer_wl(self) -> float:
        """side sail area of a ship with summer load waterline draft"""
        return self.__area_side_sail_draught_summer_wl

    @area_side_sail_draught_summer_wl.setter
    def area_side_sail_draught_summer_wl(self, value: float | str | int | None) -> None:
        self.__area_side_sail_draught_summer_wl = string_to_float(value)

    @property
    def volume_design(self) -> float:
        """volume at the construction waterline"""
        return self.__volume_design

    @volume_design.setter
    def volume_design(self, value: float | str | int | None) -> None:
        self.__volume_design = string_to_float(value)

    @property
    def volume_summer_wl(self) -> float:
        """volume at the summer load waterline"""
        return self.__volume_summer_wl

    @volume_summer_wl.setter
    def volume_summer_wl(self, value: float | str | int | None) -> None:
        self.__volume_summer_wl = string_to_float(value)

    @property
    def x_g_area_side_sail_draught_min_rudder(self) -> float:
        """horizontal distance from midship frame (middle of length L) to center of gravity area a_3, m.
        The value x_0 is taken positive when the gravity center is located in the bow from the midship frame
         and negative - in the stern"""
        return self.__x_g_area_side_sail_draught_min_rudder

    @x_g_area_side_sail_draught_min_rudder.setter
    def x_g_area_side_sail_draught_min_rudder(self, value: float | str | int | None) -> None:
        self.__x_g_area_side_sail_draught_min_rudder = string_to_float(value)

    @property
    def x_g_area_side_sail_draught_summer_wl(self) -> float:
        """horizontal distance from the midship frame (the middle of the length L1) to the center the gravity of
        the area A_5, m. The value x is taken positive when the location center of gravity in the bow from the
        midship frame and negative - in the stern"""
        return self.__x_g_area_side_sail_draught_summer_wl

    @x_g_area_side_sail_draught_summer_wl.setter
    def x_g_area_side_sail_draught_summer_wl(self, value: float | str | int | None) -> None:
        self.__x_g_area_side_sail_draught_summer_wl = string_to_float(value)

    @property
    def area_stern_valance_summer_wl(self) -> float:
        """area of the lateral projection of the vessel's stern valance, m2,
        calculated as area a figure bounded by a continuation line of the lower edge of the keel, a
        perpendicular, lowered onto this line from the point of intersection of the summer load waterline
        with the contour of the diametrical section of the stern end of the vessel, and the outer
        the edge of the stern post, drawn without taking into account the rudder post, the sole of the stern post
        or steering wheel bracket, if any"""
        return self.__area_stern_valance_summer_wl

    @area_stern_valance_summer_wl.setter
    def area_stern_valance_summer_wl(self, value: float | str | int | None) -> None:
        self.__area_stern_valance_summer_wl = string_to_float(value)

    @property
    def area_blades_on_stern_valance_summer_wl(self) -> float:
        """For twin-screw ships - the area of the lateral projection of the
               propeller fairings (or part of it) superimposed on the area of the figure"""
        return self.__area_blades_on_stern_valance_summer_wl

    @area_blades_on_stern_valance_summer_wl.setter
    def area_blades_on_stern_valance_summer_wl(self, value: float | str | int | None) -> None:
        self.__area_blades_on_stern_valance_summer_wl = string_to_float(value)

    @property
    def hull_source(self) -> str:
        """specify file or direct input (manual)"""
        return self.__hull_source

    @hull_source.setter
    def hull_source(self, value):
        hull_sources = ['manual']
        value = str(value).lower().strip()
        if value in hull_sources or value[-4:] == '.s2g':
            self.__hull_source = value
        else:
            raise Exception(f'Hull source not in {hull_sources}')

    @property
    def cf_b_design(self) -> float:
        """block coefficient at draft at construction waterline"""
        return round(self.volume_design / (self.length_wl * self.breadth_design * self.draught_design), 4)

    @property
    def cf_b_summer_wl(self) -> float:
        """block coefficient at draft at the summer load waterline"""
        return round(self.volume_summer_wl / (self.length_summer_wl * self.breadth_summer_wl *
                                              self.draught_summer_wl), 4)

    @property
    def cf_m_design(self) -> float:
        """coefficient of completeness of the midship frame at design draft"""
        return round(self.area_midship_design / (self.draught_design * self.breadth_design), 4)

    @property
    def cf_m_summer_wl(self) -> float:
        """coefficient of completeness of the midship frame at draft at the summer load waterline"""
        return round(self.area_midship_summer_wl / (self.draught_summer_wl * self.breadth_summer_wl), 4)

    @property
    def cf_p_design(self) -> float:
        """coefficient of completeness of the underwater part of the ship's hull at construction draft"""
        return round(self.cf_b_design / self.cf_m_design, 4)

    @property
    def cf_p_summer_wl(self) -> float:
        """coefficient of completeness of the underwater part of the ship's hull at
         draft at the summer load waterline"""
        return round(self.cf_b_summer_wl / self.cf_m_summer_wl, 4)

    @classmethod
    def instantiate_from_attributes(cls, attributes: dict):
        """
        Creates an instance of a hull from attributes

        :param attributes: dict of instance attributes
        """

        title = attributes['title']
        hull_source = attributes['hull_source']
        length_oa = attributes['dimensions']['length_oa']
        length_pp = attributes['dimensions']['length_pp']
        length_wl = attributes['dimensions']['length_wl']
        length_summer_wl = attributes['dimensions']['length_summer_wl']
        breadth_oa = attributes['dimensions']['breadth_oa']
        breadth_design = attributes['dimensions']['breadth_design']
        breadth_summer_wl = attributes['dimensions']['breadth_summer_wl']
        draught_design = attributes['dimensions']['draught_design']
        draught_summer_wl = attributes['dimensions']['draught_summer_wl']
        draught_min_rudder = attributes['dimensions']['draught_min_rudder']
        area_diam_butt_summer_wl = attributes['dimensions']['area_diam_butt_summer_wl']
        area_diam_butt_draught_min_rudder = attributes['dimensions']['area_diam_butt_draught_min_rudder']
        area_midship_design = attributes['dimensions']['area_midship_design']
        area_midship_summer_wl = attributes['dimensions']['area_midship_summer_wl']
        area_side_sail_draught_min_rudder = attributes['dimensions']['area_side_sail_draught_min_rudder']
        area_side_sail_draught_summer_wl = attributes['dimensions']['area_side_sail_draught_summer_wl']
        volume_design = attributes['dimensions']['volume_design']
        volume_summer_wl = attributes['dimensions']['volume_summer_wl']
        x_g_area_side_sail_draught_min_rudder = attributes['dimensions']['x_g_area_side_sail_draught_min_rudder']
        x_g_area_side_sail_draught_summer_wl = attributes['dimensions']['x_g_area_side_sail_draught_summer_wl']
        area_stern_valance_summer_wl = attributes['dimensions']['area_stern_valance_summer_wl']
        area_blades_on_stern_valance_summer_wl = attributes['dimensions']['area_blades_on_stern_valance_summer_wl']

        return cls(title=title, length_oa=length_oa, length_pp=length_pp, length_wl=length_wl,
                   length_summer_wl=length_summer_wl, breadth_oa=breadth_oa, breadth_design=breadth_design,
                   breadth_summer_wl=breadth_summer_wl, hull_source=hull_source,
                   draught_design=draught_design, draught_summer_wl=draught_summer_wl,
                   draught_min_rudder=draught_min_rudder, area_diam_butt_summer_wl=area_diam_butt_summer_wl,
                   area_diam_butt_draught_min_rudder=area_diam_butt_draught_min_rudder,
                   area_midship_design=area_midship_design, area_midship_summer_wl=area_midship_summer_wl,
                   area_side_sail_draught_min_rudder=area_side_sail_draught_min_rudder,
                   area_side_sail_draught_summer_wl=area_side_sail_draught_summer_wl,
                   volume_design=volume_design, volume_summer_wl=volume_summer_wl,
                   x_g_area_side_sail_draught_min_rudder=x_g_area_side_sail_draught_min_rudder,
                   x_g_area_side_sail_draught_summer_wl=x_g_area_side_sail_draught_summer_wl,
                   area_stern_valance_summer_wl=area_stern_valance_summer_wl,
                   area_blades_on_stern_valance_summer_wl=area_blades_on_stern_valance_summer_wl
                   )

    @classmethod
    def instantiate_from_s2g(cls, filename: str, title: str = None, draught_design: float = None,
                             draught_summer_wl: float = None, x_of_rudder_shaft: float = None,
                             propellers_quantity: int = None):
        """
        Creates an instance of a hull from s2g class

        :param filename: name of the s2g file located in ...data/ship/hull/s2g_hulls
        :param title: title of the hull
        :param draught_design: construction draught
        :param draught_summer_wl: to summer waterline
        :param x_of_rudder_shaft: distance from midship to the rudder shaft positive when the shaft is located
               in the bow from the midship frame and negative - in the stern
        :param propellers_quantity: quantity of propellers of the ship
        """

        new_s2g_hull = HullFromS2G(filename=filename, title=title, draught_design=draught_design,
                                   draught_summer_wl=draught_summer_wl)

        if not title:
            title = new_s2g_hull.title.strip('_s2g')
        hull_source = new_s2g_hull.filename
        draught_design = draught_design
        draught_summer_wl = draught_summer_wl
        draught_min_rudder = new_s2g_hull.draught_min_rudder(x_of_rudder_shaft=x_of_rudder_shaft)
        length_oa = new_s2g_hull.length_oa
        length_pp = None
        length_wl = new_s2g_hull.calculate_length(level=draught_design)
        length_summer_wl = new_s2g_hull.calculate_length(level=draught_summer_wl)
        breadth_oa = new_s2g_hull.breadth_oa
        breadth_design = new_s2g_hull.calculate_breadth(level=draught_design)
        breadth_summer_wl = new_s2g_hull.calculate_breadth(level=draught_summer_wl)
        area_diam_butt_summer_wl = new_s2g_hull.calculate_diam_buttock_area_under_water(
            level=draught_summer_wl)
        area_diam_butt_draught_min_rudder = new_s2g_hull.calculate_diam_buttock_area_under_water(
            level=draught_min_rudder)
        area_midship_design = new_s2g_hull.calculate_frame_area(x=new_s2g_hull.x_middle,
                                                                level=draught_design)
        area_midship_summer_wl = new_s2g_hull.calculate_frame_area(x=new_s2g_hull.x_middle,
                                                                   level=draught_summer_wl)
        area_side_sail_draught_min_rudder = new_s2g_hull.calculate_diam_buttock_area_upon_water(
            level=draught_min_rudder)
        area_side_sail_draught_summer_wl = new_s2g_hull.calculate_diam_buttock_area_upon_water(
            level=draught_summer_wl)
        volume_design = new_s2g_hull.calculate_volume(level=draught_design)
        volume_summer_wl = new_s2g_hull.calculate_volume(level=draught_summer_wl)
        x_g_area_side_sail_draught_min_rudder = new_s2g_hull.diam_buttock_x_0_upon_water(
            level=draught_design)
        x_g_area_side_sail_draught_summer_wl = new_s2g_hull.diam_buttock_x_0_upon_water(
            level=draught_summer_wl)
        area_stern_valance_summer_wl = new_s2g_hull.calculate_stern_valance_area(
            level=draught_summer_wl)
        area_blades_on_stern_valance_summer_wl = new_s2g_hull.get_area_blades_on_stern_valance_summer_wl(
            propellers_quantity=propellers_quantity)

        return cls(title=title, length_oa=length_oa, length_pp=length_pp, length_wl=length_wl,
                   length_summer_wl=length_summer_wl, breadth_oa=breadth_oa, breadth_design=breadth_design,
                   breadth_summer_wl=breadth_summer_wl, hull_source=hull_source,
                   draught_design=draught_design, draught_summer_wl=draught_summer_wl,
                   draught_min_rudder=draught_min_rudder, area_diam_butt_summer_wl=area_diam_butt_summer_wl,
                   area_diam_butt_draught_min_rudder=area_diam_butt_draught_min_rudder,
                   area_midship_design=area_midship_design, area_midship_summer_wl=area_midship_summer_wl,
                   area_side_sail_draught_min_rudder=area_side_sail_draught_min_rudder,
                   area_side_sail_draught_summer_wl=area_side_sail_draught_summer_wl,
                   volume_design=volume_design, volume_summer_wl=volume_summer_wl,
                   x_g_area_side_sail_draught_min_rudder=x_g_area_side_sail_draught_min_rudder,
                   x_g_area_side_sail_draught_summer_wl=x_g_area_side_sail_draught_summer_wl,
                   area_stern_valance_summer_wl=area_stern_valance_summer_wl,
                   area_blades_on_stern_valance_summer_wl=area_blades_on_stern_valance_summer_wl)

    def get_attributes(self) -> dict:
        """
        Collects attributes from an instance

        :return: dict of attributes
        """

        attributes = {
            'title': self.title,
            'hull_source': self.hull_source,
            'dimensions': {
                'length_oa': str(self.length_oa),
                'length_pp': str(self.length_pp),
                'length_wl': str(self.length_wl),
                'length_summer_wl': str(self.length_summer_wl),
                'breadth_oa': str(self.breadth_oa),
                'breadth_design': str(self.breadth_design),
                'breadth_summer_wl': str(self.breadth_summer_wl),
                'draught_design': str(self.draught_design),
                'draught_summer_wl': str(self.draught_summer_wl),
                'draught_min_rudder': str(self.draught_min_rudder),
                'area_diam_butt_summer_wl': str(self.area_diam_butt_summer_wl),
                'area_diam_butt_draught_min_rudder': str(self.area_diam_butt_draught_min_rudder),
                'area_midship_design': str(self.area_midship_design),
                'area_midship_summer_wl': str(self.area_midship_summer_wl),
                'area_side_sail_draught_min_rudder': str(self.area_side_sail_draught_min_rudder),
                'area_side_sail_draught_summer_wl': str(self.area_side_sail_draught_summer_wl),
                'volume_design': str(self.volume_design),
                'volume_summer_wl': str(self.volume_summer_wl),
                'x_g_area_side_sail_draught_min_rudder': str(self.x_g_area_side_sail_draught_min_rudder),
                'x_g_area_side_sail_draught_summer_wl': str(self.x_g_area_side_sail_draught_summer_wl),
                'area_stern_valance_summer_wl': str(self.area_stern_valance_summer_wl),
                'area_blades_on_stern_valance_summer_wl': str(self.area_blades_on_stern_valance_summer_wl)
            }
        }
        return attributes
