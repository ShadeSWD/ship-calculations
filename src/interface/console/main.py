from src.ship.ship import Ship
from src.ship.hull.hull import Hull
from src.ship.hull.hull_s2g import HullFromS2G
from src.ship.devices.rudder.rudder import Rudder
from src.utils import json_instance

if __name__ == "__main__":
    new_ship = Ship()
    new_s2g_hull = HullFromS2G(filename='SDC-1481.s2g')
    new_rudder = Rudder()

    new_ship.title = 'SDC-1481 ship'
    new_ship.ship_type = 'general'
    new_ship.propellers_quantity = 1
    new_ship.velocity_design = 14
    new_ship.x_of_rudder_shaft = -62
    new_hull = Hull.instantiate_from_s2g(filename='SDC-1481.s2g', draught_design=8.9, draught_summer_wl=9,
                                         x_of_rudder_shaft=int(new_ship.x_of_rudder_shaft),
                                         propellers_quantity=new_ship.propellers_quantity)
    new_ship.rudder = new_rudder
    new_ship.hull = new_hull

    new_rudder.title = 'SDC rudder'
    new_rudder.rudder_type = 1
    new_rudder.rudder_blade_area = 35
    new_rudder.rudderpost_area = 0
    new_rudder.rudder_med_height = 7

    new_s2g_hull.draught_design = 8.9
    new_s2g_hull.draught_summer_wl = 9

    print(repr(new_ship))
    print(repr(new_hull))
    print(repr(new_s2g_hull))
    print(repr(new_rudder))

    json_instance.save_to_json(new_ship)
    json_instance.save_to_json(new_hull)
    json_instance.save_to_json(new_rudder)
    json_instance.save_to_json(new_s2g_hull)

    print(new_ship.calculate_rudder_efficiencies())
