import json
from os import path


def save_to_json(instance) -> None:
    """
    Collects attributes from an instance and saves to json

    :param instance: instance of a class
    """
    attributes = instance.get_attributes()
    with open(path.normpath(rf"{instance.data_dir}/{instance.title}.json"), "w+") as write_file:
        json.dump(attributes, write_file, indent=2)


def read_from_json(file: [str | bytes]) -> dict:
    """
    Reads data from json and returns attributes of an instance

    :param file: path to json file
    :return: dict with instance attributes
    """
    with open(file, 'r') as json_file:
        instance = json.load(json_file)
    return instance
