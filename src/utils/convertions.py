def string_to_float(line: str | float | int | None) -> float | None:
    """
    Converts string to float

    :param line: number in str format
    :return: float or 0 if 'None' or 'null'
    """
    if line is None:
        return None
    else:
        try:
            return float(line)
        except ValueError:
            if line.lower() == 'none' or 'null':
                return 0


def string_to_int(line: str | float | int | None) -> int | None:
    """
    Converts string to int

    :param line: number in str format
    :return: int or 0 if 'None' or 'null'
    """
    if line is None:
        return None
    else:
        try:
            return int(float(line))
        except ValueError:
            if line.lower() == 'none' or 'null':
                return 0
