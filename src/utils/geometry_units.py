class Point:
    """Represents a 3D point"""
    def __init__(self, x: float | int = None, y: float | int = None, z: float | int = None) -> None:
        """
        Creates an instance of a point
        :param x: coordinate x
        :param y: coordinate y
        :param z: coordinate z
        """
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f'[{self.x}, {self.y}, {self.z}]'

    def __repr__(self):
        return f'Point(x={self.x}, y={self.y}, z={self.z})'
