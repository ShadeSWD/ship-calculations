import math
from collections import namedtuple


def interpolate(x1: float, y1: float, x2: float, y2: float, x3: float) -> float:
    """
    Interpolates coordinates of two full known and one partially known points and returns a missing coordinate
    :param x1: x of first point
    :param y1: y of first point
    :param x2: x of second point
    :param y2: y of second point
    :param x3: x of unknown third point
    :return: y of unknown third point
    """

    try:
        y3 = y1 + ((x3 - x1) * (y2 - y1)) / (x2 - x1)
    except ZeroDivisionError:
        y3 = (y1 + y2) / 2
    y3 = round(y3, 4)
    return y3


def calculate_triangle_area(x1: float, y1: float, x2: float, y2: float, x3: float, y3: float) -> float:
    """
    Calculates area of triangle by its three points
    :param x1: x of first point
    :param y1: y of first point
    :param x2: x of second point
    :param y2: y of second point
    :param x3: x of third point
    :param y3: y of third point
    :return: area of triangle
    """

    area = 0.5 * abs((x2 - x3) * (y1 - y3) - (x1 - x3) * (y2 - y3))
    area = round(area, 4)
    return area


def calculate_distance_pts(x1: int | float, y1: int | float, x2: int | float, y2: int | float) -> float:
    """
    Calculates distance between points with Pythagorean theorem
    :param x1: x of the first point
    :param y1: y of the first point
    :param x2: x of the second point
    :param y2: y of the second point
    :return: distance
    """

    distance = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    distance = round(distance, 4)
    return distance


def calculate_angle_pts(x1: int | float, y1: int | float, x2: int | float, y2: int | float, x3: int | float,
                        y3: int | float) -> float:
    """
    Calculates angle between two lines with one common point
    :param x1: x of end one
    :param y1: y of end one
    :param x2: x of the center
    :param y2: y of the center
    :param x3: x of the end two
    :param y3: y of the end two
    :return: angle in degrees
    """

    try:
        angle = math.acos(((x2 - x1) * (x3 - x2) + (y2 - y1) * (y3 - y2)) /
                          (calculate_distance_pts(x1=x1, y1=y1, x2=x2, y2=y2) *
                           calculate_distance_pts(x1=x2, y1=y2, x2=x3, y2=y3)))
    except ZeroDivisionError:
        angle = 0
    except ValueError:
        angle = 0

    angle = math.degrees(angle)
    angle = round(angle, 2)
    return angle


def collect_point_distances(mixed_coords: list) -> list:
    """
    Calculates distances between all points in the list and returns a list with distances
    :param mixed_coords: list of coords in format [{'x': 0, 'y': 0}, {}...]
    :return: list of coords in format [{'pt1': {'x': 0, 'y': 0}, 'pt_2': {'x': 0, 'y': 0}, 'dist': 0}, {}...]
    """

    points_distances = []
    kx, ky = mixed_coords[0].keys()

    try:
        for pt_1 in mixed_coords:
            for pt_2 in mixed_coords:
                dist = calculate_distance_pts(x1=pt_1[kx], y1=pt_1[ky], x2=pt_2[kx], y2=pt_2[ky])
                data = {'pt_1': {'x': pt_1[kx], 'y': pt_1[ky]}, 'pt_2': {'x': pt_2[kx], 'y': pt_2[ky]}, 'dist': dist}
                points_distances.append(data)
    except IndexError:
        pass
    return points_distances


def get_dist_btw_pts(points_distances: list, pt_1: dict, pt_2: dict) -> float:
    """
    Finds distance on a ready list of distances
    :param points_distances: list of points [{'pt1': {'x': 0, 'y': 0}, 'pt_2': {'x': 0, 'y': 0}, 'dist': 0}, {...}]
    :param pt_1: first point to find distance
    :param pt_2: second point
    :return: distance between points if found
    """

    for line in points_distances:
        line_pt_1 = [val for val in line['pt_1'].values()]
        line_pt_2 = [val for val in line['pt_2'].values()]
        cur_pt_1 = [val for val in pt_1.values()]
        cur_pt_2 = [val for val in pt_2.values()]

        if line_pt_1 == cur_pt_1 and line_pt_2 == cur_pt_2:
            return line['dist']
    raise Exception(f'distance does not exist, {pt_1}, {pt_2}\n{points_distances}')


def sort_pts_into_polygon_by_dist_and_ang(mixed_coords: list) -> list:
    """
    Sorts points into polygon using distance between points and angles between neighbour lines
    :param mixed_coords: list of coords in format [{'x': 0, 'y': 0}, {}...]
    :return: list of sorted coords in format [{'x': 0, 'y': 0}, {}...]
    """

    angle_weight = 0.021

    kx, ky = mixed_coords[0].keys()

    sorted_coords = []
    points_distances = collect_point_distances(mixed_coords=mixed_coords)

    start_point = mixed_coords[0]
    sorted_coords.append(start_point)
    second_point = None

    dist_to_start = 1000
    for pt in mixed_coords:
        dist = get_dist_btw_pts(points_distances=points_distances, pt_1=start_point, pt_2=pt)
        if dist < dist_to_start and dist != 0:
            dist_to_start = dist
            second_point = pt
    sorted_coords.append(second_point)

    while len(sorted_coords) < len(mixed_coords):
        points_values = []
        for pt in mixed_coords:
            if pt not in sorted_coords:
                pt_1 = sorted_coords[-2]
                pt_2 = sorted_coords[-1]
                pt_3 = pt

                angle = calculate_angle_pts(x1=pt_1[kx], y1=pt_1[ky], x2=pt_2[kx], y2=pt_2[ky],
                                            x3=pt_3[kx], y3=pt_3[ky])
                dist_btw_pts = get_dist_btw_pts(points_distances=points_distances, pt_1=pt_3, pt_2=pt_2)
                value = dist_btw_pts + angle_weight * angle
                data = {'pt': pt_3, 'angle': angle, 'dist_btw_pts': dist_btw_pts, 'value': value}
                points_values.append(data)
        next_point = None
        min_value = 1000

        for point in points_values:
            pt_value = point['value']
            if pt_value < min_value:
                min_value = pt_value
                next_point = point['pt']
        sorted_coords.append(next_point)

    return sorted_coords


def calculate_polygon_square(polygon_coords: list) -> float:
    """
    Calculates area of a polygon
    :param polygon_coords: 2d coordinates
    :return: area
    """

    square = 0
    i = 0
    while i < len(polygon_coords):
        try:
            square += 0.5 * (polygon_coords[i][0] * polygon_coords[i + 1][1] -
                             polygon_coords[i][1] * polygon_coords[i + 1][0])
            i += 1
        except IndexError:
            square += 0.5 * (polygon_coords[i][0] * polygon_coords[0][1] - polygon_coords[i][1] * polygon_coords[0][0])
            i += 1
    square = abs(square)
    return square


def calculate_x_0(polygon_coords: list, z_max: int | float) -> float:
    """
    Finds x_c of a polygon
    :param polygon_coords: list of 2d coords
    :param z_max: horizontal level
    :return: x_c
    """

    cm = []
    left_coords = []
    right_coords = []
    for coord in polygon_coords:
        if coord[0] <= 0:
            left_coords.append(coord)
        else:
            right_coords.append(coord)
    right_coords.reverse()
    height_bit = z_max / 1000
    lcm = namedtuple('LCM', 'x z')
    rcm = namedtuple('RCM', 'x z')
    curr_line = 0
    while curr_line < z_max:
        min_coord, max_coord = find_neighbours(half_polygon_coords=left_coords, z=curr_line)
        x_c = interpolate(x1=min_coord[1], y1=min_coord[0], x2=max_coord[1], y2=max_coord[0], x3=curr_line)
        cm.append(lcm(x=x_c, z=curr_line))

        min_coord, max_coord = find_neighbours(half_polygon_coords=right_coords, z=curr_line)
        x_c = interpolate(x1=min_coord[1], y1=min_coord[0], x2=max_coord[1], y2=max_coord[0], x3=curr_line)
        cm.append(rcm(x=x_c, z=curr_line))

        curr_line += height_bit
    sx = 0
    sum_length = 0
    for elem in cm:
        sx += abs(elem.x) * (elem.x / 2)
        sum_length += abs(elem.x)
    x_c = sx / sum_length
    return x_c


def find_neighbours(half_polygon_coords: list, z: int | float) -> tuple:
    """
    Finds neighbour points in polygon near the horizontal level
    :param half_polygon_coords: list of 2d coords
    :param z: horizontal level
    :return: min and max coords
    """

    i = 0
    max_coord = None
    min_coord = None
    while i < len(half_polygon_coords):
        if half_polygon_coords[i][1] > z:
            i += 1
        else:
            max_coord = half_polygon_coords[i - 1]
            min_coord = half_polygon_coords[i]
            break
    return min_coord, max_coord
