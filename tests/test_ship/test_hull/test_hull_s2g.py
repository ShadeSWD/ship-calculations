from src.ship.hull.hull_s2g import HullFromS2G
from src.utils.geometry_units import Point
import pytest


@pytest.fixture()
def hull_1():
    hull_1 = HullFromS2G(filename='SDC-1481.s2g', title='SDC-1481', draught_design=8.9, draught_summer_wl=9)
    return hull_1


def test_hull_s2g_init(hull_1):
    assert hull_1.filename == 'SDC-1481.s2g'
    assert hull_1.title == 'SDC-1481'
    assert hull_1.hull_data[0].dn == 25.0


def test_hull_s2g_init_empty():
    hull = HullFromS2G(filename='SDC-1481.s2g')
    assert hull.title == 'SDC-1481_s2g'


def test_hull_s2g_str(hull_1):
    assert str(hull_1) == 'SDC-1481 from s2g'


def test_hull_s2g_get_frame_coords_x(hull_1):
    assert hull_1.get_frame_coords_x(x_fr=17.165) == [Point(x=17.165, y=0.0, z=0.0),
                                                      Point(x=17.165, y=7.098, z=0.207),
                                                      Point(x=17.165, y=8.533, z=0.565),
                                                      Point(x=17.165, y=9.792, z=1.162),
                                                      Point(x=17.165, y=10.538, z=1.739),
                                                      Point(x=17.165, y=11.367, z=2.736),
                                                      Point(x=17.165, y=11.8, z=3.487),
                                                      Point(x=17.165, y=12.086, z=4.268),
                                                      Point(x=17.165, y=12.438, z=5.229),
                                                      Point(x=17.165, y=12.622, z=6.02),
                                                      Point(x=17.165, y=12.744, z=6.971),
                                                      Point(x=17.165, y=12.79, z=7.771),
                                                      Point(x=17.165, y=12.799, z=8.714),
                                                      Point(x=17.165, y=12.8, z=12.2),
                                                      Point(x=17.165, y=0.0, z=12.2)]


# def test_hull_s2g_get_frame_coords_z(hull_1):
#     assert (hull_1.get_frame_coords_z(
#         fr_coords=[{'y': 0.0, 'z': 0.0}, {'y': 7.098, 'z': 0.207}, {'y': 8.533, 'z': 0.565}, {'y': 9.792, 'z': 1.162},
#                    {'y': 10.538, 'z': 1.739}, {'y': 11.367, 'z': 2.736}, {'y': 11.8, 'z': 3.487},
#                    {'y': 12.086, 'z': 4.268}, {'y': 12.438, 'z': 5.229}, {'y': 12.622, 'z': 6.02},
#                    {'y': 12.744, 'z': 6.971}, {'y': 12.79, 'z': 7.771}, {'y': 12.799, 'z': 8.714},
#                    {'y': 12.8, 'z': 12.2}, {'y': 0.0, 'z': 12.2}], level=7) == [{'y': 0.0, 'z': 0.0},
#                                                                                 {'y': 7.098, 'z': 0.207},
#                                                                                 {'y': 8.533, 'z': 0.565},
#                                                                                 {'y': 9.792, 'z': 1.162},
#                                                                                 {'y': 10.538, 'z': 1.739},
#                                                                                 {'y': 11.367, 'z': 2.736},
#                                                                                 {'y': 11.8, 'z': 3.487},
#                                                                                 {'y': 12.086, 'z': 4.268},
#                                                                                 {'y': 12.438, 'z': 5.229},
#                                                                                 {'y': 12.622, 'z': 6.02},
#                                                                                 {'y': 12.744, 'z': 6.971},
#                                                                                 {'y': 12.7457, 'z': 7},
#                                                                                 {'y': 0, 'z': 7}])


def test_hull_s2g_get_attributes(hull_1):
    assert hull_1.get_attributes() == {'filename': 'SDC-1481.s2g', 'title': 'SDC-1481',
                                       'dimensions': {'draught_design': '8.9', 'draught_summer_wl': '9'}}


def test_hull_instantiate_from_attributes():
    hull = HullFromS2G.instantiate_from_attributes(attributes={'filename': 'SDC-1481.s2g', 'title': 'SDC-1481',
                                                               'dimensions': {'draught_design': '8.9',
                                                                              'draught_summer_wl': '9'}})
    assert hull.filename == 'SDC-1481.s2g'
    assert hull.title == 'SDC-1481'
    assert hull.hull_data[0].dn == 8
