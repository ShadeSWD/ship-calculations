import pytest
from src.ship.hull.hull import Hull


@pytest.fixture()
def hull_1():
    hull = Hull(title="test_hull_1", hull_source='manual', length_oa=100, length_pp=90, length_wl=95,
                length_summer_wl=98, breadth_oa=10, breadth_design=7, breadth_summer_wl=8, draught_design=7,
                draught_summer_wl=8, draught_min_rudder=6, area_diam_butt_summer_wl=549,
                area_diam_butt_draught_min_rudder=500, area_midship_design=40,
                area_midship_summer_wl=51, area_side_sail_draught_min_rudder=400,
                area_side_sail_draught_summer_wl=351, volume_design=3600, volume_summer_wl=4900,
                x_g_area_side_sail_draught_min_rudder=-10, x_g_area_side_sail_draught_summer_wl=-9)
    return hull


def test_hull_init(hull_1):
    assert hull_1.title == "test_hull_1"
    assert hull_1.hull_source == 'manual'
    assert hull_1.length_oa == 100
    assert hull_1.length_pp == 90
    assert hull_1.length_wl == 95
    assert hull_1.length_summer_wl == 98
    assert hull_1.breadth_oa == 10
    assert hull_1.breadth_design == 7
    assert hull_1.breadth_summer_wl == 8
    assert hull_1.draught_design == 7
    assert hull_1.draught_summer_wl == 8
    assert hull_1.draught_min_rudder == 6
    assert hull_1.area_diam_butt_summer_wl == 549
    assert hull_1.area_diam_butt_draught_min_rudder == 500
    assert hull_1.area_midship_design == 40
    assert hull_1.area_midship_summer_wl == 51
    assert hull_1.area_side_sail_draught_min_rudder == 400
    assert hull_1.area_side_sail_draught_summer_wl == 351
    assert hull_1.volume_design == 3600
    assert hull_1.volume_summer_wl == 4900
    assert hull_1.x_g_area_side_sail_draught_min_rudder == -10
    assert hull_1.x_g_area_side_sail_draught_summer_wl == -9


def test_hull_init_empty():
    hull = Hull()
    assert hull.title is None


def test_hull_str(hull_1):
    assert str(hull_1) == "test_hull_1"


def test_hull_repr(hull_1):
    assert repr(hull_1) == "Hull({'title': 'test_hull_1', 'hull_source': 'manual', 'dimensions': " \
                           "{'length_oa': '100.0', 'length_pp': '90.0', 'length_wl': '95.0', " \
                           "'length_summer_wl': '98.0', 'breadth_oa': '10.0', 'breadth_design': '7.0', " \
                           "'breadth_summer_wl': '8.0', 'draught_design': '7.0', 'draught_summer_wl': " \
                           "'8.0', 'draught_min_rudder': '6.0', 'area_diam_butt_summer_wl': '549.0', " \
                           "'area_diam_butt_draught_min_rudder': '500.0', 'area_midship_design': '40.0', " \
                           "'area_midship_summer_wl': '51.0', 'area_side_sail_draught_min_rudder': " \
                           "'400.0', 'area_side_sail_draught_summer_wl': '351.0', 'volume_design': " \
                           "'3600.0', 'volume_summer_wl': '4900.0', " \
                           "'x_g_area_side_sail_draught_min_rudder': '-10.0', " \
                           "'x_g_area_side_sail_draught_summer_wl': '-9.0'}})"


def test_hull_get_attributes(hull_1):
    assert hull_1.get_attributes() == {'title': 'test_hull_1', 'hull_source': 'manual',
                                       'dimensions': {'area_diam_butt_draught_min_rudder': '500.0',
                                                      'area_diam_butt_summer_wl': '549.0',
                                                      'area_midship_design': '40.0',
                                                      'area_midship_summer_wl': '51.0',
                                                      'area_side_sail_draught_min_rudder': '400.0',
                                                      'area_side_sail_draught_summer_wl': '351.0',
                                                      'breadth_design': '7.0',
                                                      'breadth_oa': '10.0',
                                                      'breadth_summer_wl': '8.0',
                                                      'draught_design': '7.0',
                                                      'draught_min_rudder': '6.0',
                                                      'draught_summer_wl': '8.0',
                                                      'length_oa': '100.0',
                                                      'length_pp': '90.0',
                                                      'length_summer_wl': '98.0',
                                                      'length_wl': '95.0',
                                                      'volume_design': '3600.0',
                                                      'volume_summer_wl': '4900.0',
                                                      'x_g_area_side_sail_draught_min_rudder': '-10.0',
                                                      'x_g_area_side_sail_draught_summer_wl': '-9.0'}}


def test_instantiate_from_attributes():
    hull = Hull.instantiate_from_attributes({'title': 'test_hull_1', 'hull_source': 'manual',
                                             'dimensions': {'area_diam_butt_draught_min_rudder': '500.0',
                                                            'area_diam_butt_summer_wl': '549.0',
                                                            'area_midship_design': '40.0',
                                                            'area_midship_summer_wl': '51.0',
                                                            'area_side_sail_draught_min_rudder': '400.0',
                                                            'area_side_sail_draught_summer_wl': '351.0',
                                                            'breadth_design': '7.0',
                                                            'breadth_oa': '10.0',
                                                            'breadth_summer_wl': '8.0',
                                                            'draught_design': '7.0',
                                                            'draught_min_rudder': '6.0',
                                                            'draught_summer_wl': '8.0',
                                                            'length_oa': '100.0',
                                                            'length_pp': '90.0',
                                                            'length_summer_wl': '98.0',
                                                            'length_wl': '95.0',
                                                            'volume_design': '3600.0',
                                                            'volume_summer_wl': '4900.0',
                                                            'x_g_area_side_sail_draught_min_rudder': '-10.0',
                                                            'x_g_area_side_sail_draught_summer_wl': '-9.0'}})
    assert hull.title == "test_hull_1"
    assert hull.hull_source == 'manual'
    assert hull.length_oa == 100
    assert hull.length_pp == 90
    assert hull.length_wl == 95
    assert hull.length_summer_wl == 98
    assert hull.breadth_oa == 10
    assert hull.breadth_design == 7
    assert hull.breadth_summer_wl == 8
    assert hull.draught_design == 7
    assert hull.draught_summer_wl == 8
    assert hull.draught_min_rudder == 6
    assert hull.area_diam_butt_summer_wl == 549
    assert hull.area_diam_butt_draught_min_rudder == 500
    assert hull.area_midship_design == 40
    assert hull.area_midship_summer_wl == 51
    assert hull.area_side_sail_draught_min_rudder == 400
    assert hull.area_side_sail_draught_summer_wl == 351
    assert hull.volume_design == 3600
    assert hull.volume_summer_wl == 4900
    assert hull.x_g_area_side_sail_draught_min_rudder == -10
    assert hull.x_g_area_side_sail_draught_summer_wl == -9


def test_hull_properties(hull_1):
    assert hull_1.sigma_k == 0.7003
    assert hull_1.cf_b_design == 0.7734
    assert hull_1.cf_b_summer_wl == 0.7812
    assert hull_1.cf_m_design == 0.8163
    assert hull_1.cf_m_summer_wl == 0.7969
    assert hull_1.cf_p_design == 0.9474
    assert hull_1.cf_p_summer_wl == 0.9803
