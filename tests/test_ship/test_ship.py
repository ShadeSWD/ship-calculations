from src.ship.ship import Ship
import pytest


@pytest.fixture()
def ship_1():
    ship = Ship(title="test_ship_1", ship_type="cargo", propellers_quantity=1, velocity_design=7, x_of_rudder_shaft=-40,
                hull=None, rudder=None)
    return ship


def test_ship_init(ship_1):
    assert ship_1.title == "test_ship_1"
    assert ship_1.ship_type == "cargo"
    assert ship_1.propellers_quantity == 1
    assert ship_1.velocity_design == 7
    assert ship_1.x_of_rudder_shaft == -40
    assert ship_1.hull is None
    assert ship_1.rudder is None


def test_ship_init_empty():
    ship = Ship()
    assert ship.title is None


def test_ship_str(ship_1):
    assert str(ship_1) == 'test_ship_1'


def test_ship_repr(ship_1):
    assert repr(ship_1) == "Ship({'title': 'test_ship_1', 'ship_type': 'cargo', 'propellers_quantity': " \
                           "1, 'velocity_design': 7.0, 'x_of_rudder_shaft': -40.0, 'sub_parts': {'hull': " \
                           "'None', 'rudder': 'None'}})"


def test_ship_get_attributes(ship_1):
    assert ship_1.get_attributes() == {'propellers_quantity': 1,
                                       'ship_type': 'cargo',
                                       'sub_parts': {'hull': 'None', 'rudder': 'None'},
                                       'title': 'test_ship_1',
                                       'velocity_design': 7.0,
                                       'x_of_rudder_shaft': -40.0}


def test_ship_instantiate_from_attributes():
    ship = Ship.instantiate_from_attributes(attributes={'propellers_quantity': 1, 'ship_type': 'cargo',
                                                        'velocity_design': 7.0,
                                                        'x_of_rudder_shaft': -80,
                                                        'sub_parts': {'hull': 'None', 'rudder': 'None'},
                                                        'title': 'test_ship_1'})
    assert ship.title == 'test_ship_1'
