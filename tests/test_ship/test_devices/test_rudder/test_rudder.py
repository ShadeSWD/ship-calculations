import pytest
from src.ship.devices.rudder.rudder import Rudder


@pytest.fixture()
def rudder_1():
    rudder = Rudder(title="test_rudder_1", rudder_type=3, rudder_blade_area=5, rudderpost_area=0, rudder_med_height=3)
    return rudder


@pytest.fixture()
def rudder_2():
    rudder = Rudder(title="test_rudder_2", rudder_type=10, rudder_blade_area=3, rudderpost_area=1, rudder_med_height=3)
    return rudder


def test_rudder_init(rudder_1):
    assert rudder_1.title == "test_rudder_1"
    assert rudder_1.rudder_type == 3
    assert rudder_1.rudder_blade_area == 5
    assert rudder_1.rudderpost_area == 0
    assert rudder_1.rudder_med_height == 3


def test_rudder_init_empty():
    rudder = Rudder()
    assert rudder.title is None


def test_rudder_rudder_type_setter(rudder_1):
    rudder_1.rudder_type = None
    assert rudder_1.rudder_type is None
    rudder_1.rudder_type = 2
    assert rudder_1.rudder_type == 2


def test_rudder_str(rudder_1):
    assert str(rudder_1) == "test_rudder_1"


def test_rudder_repr(rudder_1):
    assert repr(rudder_1) == "Rudder({'title': 'test_rudder_1', 'rudder_type': '3', 'dimensions': " \
                             "{'rudder_blade_area': '5.0', 'rudderpost_area': '0.0', 'rudder_med_height': '3.0'}})"


def test_rudder_rudder_area_oa(rudder_1, rudder_2):
    assert rudder_1.rudder_area_oa == 5
    assert rudder_2.rudder_area_oa == 4


def test_rudder_lamda_p(rudder_1):
    assert rudder_1.lamda_p == 1.8


def test_rudder_get_attributes(rudder_1):
    assert rudder_1.get_attributes() == {'title': 'test_rudder_1', 'rudder_type': '3',
                                         'dimensions': {'rudder_blade_area': '5.0', 'rudderpost_area': '0.0',
                                                        'rudder_med_height': '3.0'}}


def test_rudder_calculate_efficiency_1(rudder_1):
    assert rudder_1.calculate_efficiency_1(cf_p=0.73, sigma_k=0.8) == 0.1038


def test_rudder_calculate_efficiency_2():
    rudder = Rudder(rudder_type=1, rudder_blade_area=10, rudder_med_height=5)
    assert rudder.calculate_efficiency_2(a_3=1600, a_4=1100, v=8, x_0=-7, l_1=138) == 0.1372


def test_rudder_calculate_efficiency_3():
    rudder = Rudder(rudder_type=1, rudder_blade_area=10, rudder_med_height=5)
    assert rudder.calculate_efficiency_3(a_2=1600, a_5=1100, x=-7, l_1=138) == 0.0529
    assert rudder.calculate_efficiency_3(a_2=1600, a_5=1100, x=-7, l_1=60) == 0
