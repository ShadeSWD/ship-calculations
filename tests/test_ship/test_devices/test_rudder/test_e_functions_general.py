from src.ship.devices.rudder.e_functions_general import *


def test_e_95():
    assert e_95(sigma=0.8) == 0.12399996260284542
    assert e_95(sigma=0.97) == 0.026255418799999982
    assert e_95(sigma=0.2) == 0


def test_e_85():
    assert e_85(sigma=0.8) == 0.11599999737402361
    assert e_85(sigma=0.97) == 0.033584966399999985
    assert e_85(sigma=0.2) == 0


def test_e_75():
    assert e_75(sigma=0.8) == 0.10599998528519998
    assert e_75(sigma=0.97) == 0.045132635669999976
    assert e_75(sigma=0.2) == 0


def test_e_65():
    assert e_65(sigma=0.8) == 0.09500001944065516
    assert e_65(sigma=0.97) == 0.05593838384319998
    assert e_65(sigma=0.2) == 0


def test_e_55():
    assert e_55(sigma=0.8) == 0.08649999548704498
    assert e_55(sigma=0.97) == 0.06622056238599998
    assert e_55(sigma=0.2) == 0


def test_get_e():
    assert get_d_e(c_p=0.55, sigma=0.8) == 0.0865
    assert get_d_e(c_p=0.65, sigma=0.8) == 0.095
    assert get_d_e(c_p=0.75, sigma=0.8) == 0.106
    assert get_d_e(c_p=0.85, sigma=0.8) == 0.116
    assert get_d_e(c_p=0.95, sigma=0.8) == 0.124
    assert get_d_e(c_p=0.2, sigma=0.8) == 0


def test_interpolate_e():
    assert interpolate_e(c_p=0.56, sigma=0.8) == 0.0873
    assert interpolate_e(c_p=0.67, sigma=0.82) == 0.0928
    assert interpolate_e(c_p=0.76, sigma=0.95) == 0.0877
    assert interpolate_e(c_p=0.91, sigma=0.9) == 0.0829
    assert interpolate_e(c_p=0.97, sigma=0.7) == 0
