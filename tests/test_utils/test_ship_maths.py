from src.utils.ship_maths import *


def test_interpolate():
    assert interpolate(x1=5, y1=3, x2=4, y2=6, x3=8) == -6
    assert interpolate(x1=0, y1=1, x2=1, y2=2, x3=2) == 3
    assert interpolate(x1=0, y1=0, x2=0, y2=0, x3=0) == 0
    assert interpolate(x1=0, y1=1, x2=0, y2=3, x3=2) == 2


def test_calculate_triangle_area():
    assert calculate_triangle_area(x1=5, y1=3, x2=4, y2=6, x3=8, y3=0) == 3.0
    assert calculate_triangle_area(x1=0, y1=0, x2=0, y2=0, x3=0, y3=0) == 0.0
    assert calculate_triangle_area(x1=0, y1=4, x2=0, y2=-1, x3=0, y3=3) == 0.0


def test_calculate_distance_pts():
    assert calculate_distance_pts(x1=0, y1=0, x2=0, y2=0) == 0
    assert calculate_distance_pts(x1=0, y1=1, x2=0, y2=0) == 1
    assert calculate_distance_pts(x1=5, y1=3, x2=-2, y2=-1) == 8.0623


def test_calculate_angle_pts():
    assert calculate_angle_pts(x1=0, y1=0, x2=0, y2=0, x3=0, y3=0) == 0
    assert calculate_angle_pts(x1=0, y1=5, x2=0, y2=0, x3=5, y3=0) == 90
    assert calculate_angle_pts(x1=4, y1=-1, x2=2, y2=5, x3=0, y3=9) == 8.13


def test_collect_point_distances():
    assert collect_point_distances([{'x': 2, 'y': 3}, {'x': 4, 'y': 2}]) == [
        {'dist': 0.0, 'pt_1': {'x': 2, 'y': 3}, 'pt_2': {'x': 2, 'y': 3}},
        {'dist': 2.2361, 'pt_1': {'x': 2, 'y': 3}, 'pt_2': {'x': 4, 'y': 2}},
        {'dist': 2.2361, 'pt_1': {'x': 4, 'y': 2}, 'pt_2': {'x': 2, 'y': 3}},
        {'dist': 0.0, 'pt_1': {'x': 4, 'y': 2}, 'pt_2': {'x': 4, 'y': 2}}]


def test_get_dist_btw_pts():
    distances = [
        {'dist': 0.0, 'pt_1': {'x': 2, 'y': 3}, 'pt_2': {'x': 2, 'y': 3}},
        {'dist': 2.2361, 'pt_1': {'x': 2, 'y': 3}, 'pt_2': {'x': 4, 'y': 2}},
        {'dist': 2.2361, 'pt_1': {'x': 4, 'y': 2}, 'pt_2': {'x': 2, 'y': 3}},
        {'dist': 0.0, 'pt_1': {'x': 4, 'y': 2}, 'pt_2': {'x': 4, 'y': 2}}]
    assert get_dist_btw_pts(points_distances=distances, pt_1={'x': 2, 'y': 3}, pt_2={'x': 4, 'y': 2}) == 2.2361


def test_sort_pts_into_polygon_by_dist_and_ang():
    unsorted_points = [{'x': 1, 'y': 1}, {'x': 2, 'y': 3}, {'x': 4, 'y': 2}, {'x': 4, 'y': -2}, {'x': 2, 'y': -2},
                       {'x': 1, 'y': -1}, {'x': 5, 'y': 1}, {'x': 6.6, 'y': -1.5}]
    assert sort_pts_into_polygon_by_dist_and_ang(mixed_coords=unsorted_points) == [{'x': 1, 'y': 1},
                                                                                   {'x': 1, 'y': -1},
                                                                                   {'x': 2, 'y': -2},
                                                                                   {'x': 4, 'y': -2},
                                                                                   {'x': 6.6, 'y': -1.5},
                                                                                   {'x': 5, 'y': 1},
                                                                                   {'x': 4, 'y': 2},
                                                                                   {'x': 2, 'y': 3}]
