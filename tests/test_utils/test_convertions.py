from src.utils.convertions import *


def test_string_to_float():
    assert string_to_float(line='None') == 0
    assert string_to_float(line='nUll') == 0
    assert string_to_float('0.556') == 0.556


def test_string_to_int():
    assert string_to_int(line='None') == 0
    assert string_to_int(line='nUll') == 0
    assert string_to_int(line='1.556') == 1
